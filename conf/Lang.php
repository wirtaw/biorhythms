<?php
class Lang{
      var $image;
      var $id;
      var $vocalbury=array();
      var $title;
      public function Set($lng){
	   
      }
      public function Show(){
	    if(file_exists($this->image)&&strcmp($this->id,$_SESSION['visitor']['lang'])!==0){
		  $info=GetImageSize($this->image);
		  return '<img src="'.$this->image.'" onclick="javascript:select_lang(\''.$this->id.'\');" width="'.$info[0].'" height="'.$info[1].'" style="width:'.$info[0].'px;height:'.$info[1].'px;padding:2px;margin:2px;float:left;cursor:pointer;" alt="lang'.$this->id.'">';
	    }elseif(!file_exists($this->image)&&strcmp($this->id,$_SESSION['visitor']['lang'])!==0){
		  return '<span onclick="javascript:select_lang(\''.$this->id.'\');">'.$this->id.'</span>';
	    }
      }
      public function Show_In($dir='../'){
	    if(file_exists($dir.$this->image)&&strcmp($this->id,$_SESSION['visitor']['lang'])!==0){
		  $info=GetImageSize($dir.$this->image);
		  return '<img src="'.$dir.$this->image.'" onclick="javascript:select_lang(\''.$this->id.'\');" width="'.$info[0].'" height="'.$info[1].'" style="width:'.$info[0].'px;height:'.$info[1].'px;padding:2px;margin:2px;float:left;cursor:pointer;" alt="lang'.$this->id.'">';
	    }elseif(!file_exists($this->image)&&strcmp($this->id,$_SESSION['visitor']['lang'])!==0){
		  return '<span onclick="javascript:select_lang(\''.$this->id.'\');">'.$this->id.'</span>';
	    }
      }
      public function Show_In_Bootstrap($dir='../'){
	    if(file_exists($dir.$this->image)&&strcmp($this->id,$_SESSION['visitor']['lang'])!==0){
		  $info=GetImageSize($dir.$this->image);
		  return '<li><a href="#" onclick="javascript:select_lang(\''.$this->id.'\');"><img src="'.$dir.$this->image.'" width="'.$info[0].'" height="'.$info[1].'" style="width:'.$info[0].'px;height:'.$info[1].'px;padding:2px;margin:2px;float:left;cursor:pointer;" alt="lang'.$this->id.'"> '.$this->id.'</a></li>';
	    }elseif(!file_exists($this->image)&&strcmp($this->id,$_SESSION['visitor']['lang'])!==0){
		  return '<li><a href="#" onclick="javascript:select_lang(\''.$this->id.'\');">'.$this->id.'</a></li>';
	    }
      }
      function Lang($title,$vocalbury,$id,$image){
	    $this->title=$title;
	    $this->vocalbury=$vocalbury;
	    $this->id=$id;
	    $this->image=$image;
      }      
}
$_SESSION['lang']=array();  
$_SESSION['lang']['en']=new Lang('English',
array(
      'title'=>'Poplauki ',
      'pagename'=>'Web Site',
      'phone'=>'Tel',
      'email'=>'Mail',
      'find'=>'Search',
      'search_phr'=>'Search Our Website&hellip;',
      'here'=>'You Are Here',
      'whoarewe'=>'Who Are We',
      'whoarewe_text'=>'I am an web developer.<br/> I work with <ul><li>HTML ( '.(date('Y')-2002).' years )</li><li>JavaScript ( '.(date('Y')-2003).' years )</li><li>CSS ( '.(date('Y')-2003).' years )</li><li>C/C++ ( '.(date('Y')-2004).' years )</li><li>PHP ( '.(date('Y')-2005).' years )</li><li>MySQL ( '.(date('Y')-2006).' years )</li><li>Java ( '.(date('Y')-2006).' years )</li></ul><br/>Interested developing PHP aplications for websites<br/>Now continue myself OOP PHP framework<br/>',
      'compatibility'=>'Compatibility',
      'compatibility_text'=>' <ul id="list">
        <li>Work by W3C standart (if it imposible)</li>
        <li>Use Javascript,CSS and PHP frameworks (if it )</li>
        <li>Work in OOP</li>
        <li>Test webaplications in Win and Unix platforms and in poplar browsers</li>
        </ul>',
      'whatwedo'=>'What We Do',
      'whatwedo_text'=>'My works:<br/><ul><li>Websites</li><li>Aplications</li><li>Modules</li><li>Tricks</li></ul>',
      'readmore'=>'Read More',
      'go'=>'Visit this',
      'download'=>'Download',
      'frmblog'=>'From The Blog',
      'follow'=>'Follow Me!',
      'wesocial'=>'We Are Social!',
      'latest'=>'Latest Work',
      'contactus'=>'Headline: Contact Us',
      'contactus_name'=>'Name (required)',
      'contactus_email'=>'Mail (required)',
      'contactus_comm'=>'Comment (required)',
      'contactus_captcha'=>'Are you bot? (required) click to reload picture',
      'submit'=>'Submit Form',
      'reset'=>'Reset Form',
      'widget'=>'Widget',
      'rights'=>'All Rights Reserved',
      'warning'=>'Warning',
      'noaccess'=>'No access!!!',
      'backto'=>'back to',
      'page'=>'page',
      'error_bot'=>'No bots please! UA reported as:',
      'error_fill'=>'Please fill in all the required fields and submit again.',
      'error_name'=>'The name field must not contain special characters.',
      'error_mail'=>'That is not a valid e-mail address.',
      'error_adress'=>'Invalid website url.',
      'error_send'=>'Your mail could not be sent this time.',
      'error_spam'=>'Your mail looks too much like spam, and could not be sent this time.',
      'error_captcha'=>'Invalid captcha.',
      'search'=>'Search results',
      'youtinput'=>'Your input -',
      'sorrysearch'=>'Sorry  context database are still updating.',
        'select'=> 'Select language'
),'en','images/gb.png');
$_SESSION['lang']['lt']=new Lang('Lithuanian',
array(
      'title'=>'Poplauki ',
      'pagename'=>'internetinis puslapis',
      'phone'=>'Tel',
      'email'=>'El. paštas',
      'find'=>'Paieška',
      'search_phr'=>'Ieškok interneto puslapyje&hellip;',
      'here'=>'Jus esate čia',
      'whoarewe'=>'Kas',
      'whoarewe_text'=>'Esu internetinių puslapių programuotojas.<br/> Dirbu su <ul><li>HTML ( '.(date('Y')-2002).' metų )</li><li>JavaScript ( '.(date('Y')-2003).' metų )</li><li>CSS ( '.(date('Y')-2003).' metų )</li><li>C/C++ ( '.(date('Y')-2004).' metų )</li><li>PHP ( '.(date('Y')-2005).' metų )</li><li>MySQL ( '.(date('Y')-2006).' metų )</li><li>Java ( '.(date('Y')-2006).' metų )</li></ul><br/>Programuoju aplikacijas internetiniams puslapiams <br/>Dabar baigiu surinkima savo OOP PHP frameworka<br/>',
      'compatibility'=>'Suderinamumas',
       'compatibility_text'=>' <ul id="list">
        <li>Dirbu pagal W3C standartus (jei sąlygos ir užduotis tai leidžia)</li>
        <li>Naudoju Javascript,CSS ir PHP frameworkus (jei tai reikainga)</li>
        <li>Dirbu OOP pagrindu</li>
        <li>Testuoju web aplikacijas su Win ir Unix platformomis ir populiariausiose naršyklese.</li>
        </ul>',
      'whatwedo'=>'Ką',
      'whatwedo_text'=>'Mano darbai:<br/><ul><li>Interneto puslapiai</li><li>Aplikacjos</li><li>Moduliai</li><li>Triukai/li></ul>',
      'readmore'=>'Skaityk daugiau',
      'go'=>'Aplankyk',
      'download'=>'Atsisiusk',
      'frmblog'=>'Iš tinklaraščio',
      'follow'=>'Sek mane!',
      'wesocial'=>'Socialiniose tinklose!',
      'latest'=>'Darbai',
      'contactus'=>'Parašyk žinutė',
      'contactus_name'=>'Vardas (būtinas)',
      'contactus_email'=>'El. paštas (būtinas)',
      'contactus_comm'=>'Tekstas (būtinas)',
      'contactus_captcha'=>'Ar tu žmogus? (būtinas) gauti nauja paveikslėlį paspausk ant jo',
      'submit'=>'Patvirtinti',
      'reset'=>'Išvalyti',
      'widget'=>'Žemėlapis',
      'rights'=>'Visos teises apgintos',
      'warning'=>'Demėsio',
      'noaccess'=>'Nėra peržiūrėjimo teisu !!!',
      'backto'=>'grįžti',
      'page'=>'tinklapis',
      'error_bot'=>'Atsiprašome, bet Jus idetfikavo kaip botą! UA informacija:',
      'error_fill'=>'Prašome visus laukelius ir vėl patvirtinti išsiuntimą',
      'error_name'=>'Vardas turi sudarytas tik iš raidžiu',
      'error_mail'=>'Toks el. pašto adresas netinkamas formos išsiuntimui',
      'error_adress'=>'Neteisingas url adresas',
      'error_send'=>'Jūsų žinutė nebuvo išsiuta dėl techninių spragu',
      'error_spam'=>'Jūsų žinutė identifikuota kaip spam ir nebuvo išsiuta',
      'error_captcha'=>'Netaisyklingas patikrinimo kodas.',
      'search'=>'Paiškos rezultatai',
      'youtinput'=>'Jūsų paiškos frazė -',
      'sorrysearch'=>'Šio metų kontento duomenų bazė atnaujinama.',
        'select'=> 'Keisti kalbą'
),'lt','images/lt.png');
$_SESSION['lang']['ru']=new Lang('Russian',
array(
      'title'=>'Poplauki  ',
      'pagename'=>'страница',
      'phone'=>'Тел.',
      'email'=>'Адрес',
      'find'=>'Поиск',
      'search_phr'=>'Искать по сайту&hellip;',
      'here'=>'Вы находитесь тут',
      'whoarewe'=>'Кто',
      'whoarewe_text'=>'Я программист веб-страниц<br/> Работаю с :<ul><li>HTML ( '.(date('Y')-2002).' лет )</li><li>JavaScript ( '.(date('Y')-2003).' лет )</li><li>CSS ( '.(date('Y')-2003).' лет )</li><li>C/C++ ( '.(date('Y')-2004).' лет )</li><li>PHP ( '.(date('Y')-2005).' лет )</li><li>MySQL ( '.(date('Y')-2006).' лет )</li><li>Java ( '.(date('Y')-2006).' лет )</li></ul><br/>Разрабатываю апликации для сайтов. <br/>В данный момент заканчиваю сборку своего OOP PHP фреймворка<br/>',
      'compatibility'=>'Многозадачность',
      'compatibility_text'=>' <ul id="list">
        <li>Работаю по W3C стандартам (если задача этому не препятствует)</li>
        <li>Использую Javascript,CSS и PHP фреймворки (если нужно)</li>
        <li>Работаю по ООП основам </li>
        <li>Тестирую веб-апликации в Win и Unix платформах и в популярных браузерах.</li>
        </ul>',
      'whatwedo'=>'Что',
      'whatwedo_text'=>'Мои работы:<br/><ul><li>Страницы</li><li>Апликации</li><li>Модули</li><li>Трюки</li></ul>',
      'readmore'=>'Читать ',
      'go'=>'Посетить',
      'download'=>'Скачать',
      'frmblog'=>'Из блога',
      'follow'=>'Следую!',
      'wesocial'=>'В социальных сетях!',
      'latest'=>'Работы',
      'contactus'=>'Написать письмо',
      'contactus_name'=>'Имя (обязательно для заполнения)',
      'contactus_email'=>'Эл. адрес (обязательно для заполнения)',
      'contactus_comm'=>'Текст (обязательно для заполнения)',
      'contactus_captcha'=>'Ты человек? (обязательно для заполнения) для новой картинки кликните на неё',
      'submit'=>'Выслать',
      'reset'=>'Вычистить',
      'widget'=>'Виджет',
      'rights'=>'Все права защищенны',
      'warning'=>'Внимание',
      'noaccess'=>'Нет прав доступа!!!',
      'backto'=>'вернуться',
      'page'=>'страница',
      'error_bot'=>'Нет ботам! UA лог:',
      'error_fill'=>'Пожалуйста заполните все поля и снова вышлите сообщение.',
      'error_name'=>'Имя должно состоять из букв.',
      'error_mail'=>'Адрес почты неверный.',
      'error_adress'=>'Неверный адрес url.',
      'error_send'=>'Ваше сообщение не может быть, на данный момент, выслано .',
      'error_spam'=>'Ваше сообщение иденфикатиция как спам и не может быть выслано .',
      'error_captcha'=>'Неверный проверочный код.',
      'search'=>'Результаты поиска',
      'youtinput'=>'Поисковая фраза -',
      'sorrysearch'=>'В данный момент база с контентом в разработке.',
        'select'=> 'Изменить язык'
),'ru','images/ru.png');
$_SESSION['lang']['pl']=new Lang('Polish',
array(
      'title'=>'Poplauki ',
      'pagename'=>'strona www',
      'phone'=>'Tel',
      'email'=>'Adres mailowy ',
      'find'=>'Szukaj',
      'search_phr'=>'Szukac na stronie&hellip;',
      'here'=>'Znajdujesz się tutaj',
      'whoarewe'=>'Kto',
      'whoarewe_text'=>'Jestem web-programista.<br/> Pracuja z :<ul><li>HTML ( '.(date('Y')-2002).' lat )</li><li>JavaScript ( '.(date('Y')-2003).' lat )</li><li>CSS ( '.(date('Y')-2003).' lat )</li><li>C/C++ ( '.(date('Y')-2004).' lat )</li><li>PHP ( '.(date('Y')-2005).' lat )</li><li>MySQL ( '.(date('Y')-2006).' lat )</li><li>Java ( '.(date('Y')-2006).' lat )</li></ul><br/>Rozpracowują aplikacje dla stron www<br/>W tym czasie  zbieram swój OOP PHP framework<br/>',
      'compatibility'=>'Spójność',
       'compatibility_text'=>' <ul id="list">
       <li>Pracuje według W3C standard (jeżeli zadanie i narzędzia to dopuszczają)</li>
        <li>Stosują Javascript,CSS i PHP frameworki (jeżeli to jest potrzebowano)</li>
        <li>Pracuje na OOP standard</li>
        <li>Testują web aplikacji z Win i Unix platformami i w popularnych przeglądarkach.</li>
        </ul>',
      'whatwedo'=>'Co',
      'whatwedo_text'=>'Moje pracy:<br/><ul><li>Strony www</li><li>Aplikacji</li><li>Moduly</li><li>Triki</li></ul>',
      'readmore'=>'Czytaj',
      'go'=>'Wizytuj',
      'download'=>'Ściągać',
      'frmblog'=>'Z bloga',
      'follow'=>'Za mną!',
      'wesocial'=>'W socialnych siecach!',
      'latest'=>'Pracy',
      'contactus'=>'Napisz ',
      'contactus_name'=>'Imie (obowjazkowy)',
      'contactus_email'=>'Adres pocztowej skrzynki (obowjazkowy)',
      'contactus_comm'=>'Tekst (obowjazkowy)',
      'contactus_captcha'=>'Czy ty człowiek? (obowjazkowy) nowy obrazek jest generowany po kliknieciu na nim',
      'submit'=>'Potwedzenie',
      'reset'=>'Reset',
      'widget'=>'Widget',
      'rights'=>'Wszystkie prawa obronione',
      'warning'=>'Uwaga',
      'noaccess'=>'Niema praw dostępu!!!',
      'backto'=>'wrócić',
      'page'=>'strona',
      'error_bot'=>'Nie mozno botam! UA reporuje ze:',
      'error_fill'=>'Prosze wypelnic wszytkie polia i wyslac ponowu.',
      'error_name'=>'Pole imie musi skladac sie z liter.',
      'error_mail'=>'Nieprawidłowy adres pocztowej skrzynki.',
      'error_adress'=>'Nieprawidłowy adres strony url.',
      'error_send'=>'Wiadomosc nie moze byc wysłana natychmiast .',
      'error_spam'=>'Ta wiadomosc jest podobna na spam i nie byla wysłana.',
      'error_captcha'=>'Nieprawidłowy kod.',
      'search'=>'Rezultaty poszukiwania',
      'youtinput'=>'Wzorzec wyszukiwania -',
      'sorrysearch'=>'Pszepraszam w tym czasie baza tresci jest w procesie budowy.',
        'select'=> 'Zmienić język'
),'pl','images/pl.png');
if(!isset($_SESSION['visitor']['lang'])){
      $_SESSION['visitor']['lang']=$_SESSION['lang']['en']->id; 
}
     // unset($_SESSION['visitor']);

if(isset($_POST['lang_sel'])) {
    $lang=trim($_POST['lang_sel']);
    $lang = stripslashes($lang);
    $lang = htmlentities($lang,ENT_NOQUOTES,'UTF-8');
    $lang = strip_tags($lang);
    if(isset($_SESSION['lang'][$lang])){
          $_SESSION['visitor']['lang']=$lang;
    }
} elseif (isset($_GET['lang'])) {
    $lang=trim($_GET['lang']);
    $lang = stripslashes($lang);
    $lang = htmlentities($lang,ENT_NOQUOTES,'UTF-8');
    $lang = strip_tags($lang);
    if(isset($_SESSION['lang'][$lang])) {
          $_SESSION['visitor']['lang']=$lang;
    }
}
function ShoWord($name,$analog){
       if(isset($_SESSION['visitor']['lang'])&&isset($_SESSION['lang'][$_SESSION['visitor']['lang']]->vocalbury[$name])){
		  return $_SESSION['lang'][$_SESSION['visitor']['lang']]->vocalbury[$name];
       }else{
		  return $analog;
       } 
}
function GetWord($name,$analog){
       if(isset($_SESSION['visitor']['lang'])&&isset($_SESSION['lang'][$_SESSION['visitor']['lang']]->vocalbury[$name])){
		  return $_SESSION['lang'][$_SESSION['visitor']['lang']]->vocalbury[$name];
       }else{
		  return $analog;
       } 
}
function ShowId(){
       if(isset($_SESSION['visitor']['lang'])){
		  echo $_SESSION['visitor']['lang'];
       }
}
function FindForm($search=''){
      if($search==''){
	    $str= '<form id="taglineform" action="search.php"><fieldset><legend>'.ShoWord('find','Search').'</legend><input name="q" id="tipue_search_input" type="text" value=""/>'
                    . '<input type="button" id="tipue_search_button"  onclick="this.form.submit();" value="&nbsp;" /></fieldset></form>';
      }else{
	     $str= '<form id="taglineform" action="search.php"><fieldset><legend>'.ShoWord('find','Search').'</legend><input name="q" id="tipue_search_input" type="text" value="'.$search.'" />'
                     . '<input type="button" id="tipue_search_button" onclick="this.form.submit();" value="&nbsp;"/></fieldset></form>';
      }
      return $str;
}
function Meta(){
        $str="\n";
	$meta=array();
	switch ($_SESSION['visitor']['lang']){
                    case 'en':
                        $meta['Content-Type']='<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
                        //$meta['Date']='<meta http-equiv="date" content="2011-07-16T22:22:22+00:00" />';
                        //$meta['Last-Modified']='<meta http-equiv="Last-Modified" content="2014-02-16T09:10:56+00:00" />';
                        $meta['Description']='<meta name="description" content="Vladimir Poplavskij Homepage" />';
                        $meta['Keywords']='<meta name="keywords" content="wirtaw,poplauki,poplavskij,php developer,travel,maps" />';
                        $meta['Author']='<meta name="author" content="Vladimir Poplavskij" />';
                        //$meta['Language']='<meta http-equiv="content-language" content="en" />';
                        $meta['Robots']='<meta name="robots" content="all" />';
                        $meta['Canonical']='<link rel="canonical" href="#" />';
                    break;
                case 'ru':
                        $meta['Content-Type']='<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
                        //$meta['Date']='<meta http-equiv="date" content="2011-07-16T22:22:22+00:00" />';
                        //$meta['Last-Modified']='<meta http-equiv="Last-Modified" content="2014-02-16T09:10:56+00:00" />';
                        $meta['Description']='<meta name="description" content="Домашняя страница Владимира Поплавского" />';
                        $meta['Keywords']='<meta name="keywords" content="владимир поплавский,программист,php,игры,разработка" />';
                        $meta['Author']='<meta name="author" content="Владимир Поплавский" />';
                        //$meta['Language']='<meta http-equiv="content-language" content="ru" />';
                        $meta['Robots']='<meta name="robots" content="all" />';
                        $meta['Canonical']='<link rel="canonical" href="#" />';
                    break;
                case 'lt':
                        $meta['Content-Type']='<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
                        //$meta['Date']='<meta http-equiv="date" content="2011-07-16T22:22:22+00:00" />';
                        //$meta['Last-Modified']='<meta http-equiv="Last-Modified" content="2014-02-16T09:10:56+00:00" />';
                        $meta['Description']='<meta name="description" content="Vladimiro Poplavskio tinklalapis" />';
                        $meta['Keywords']='<meta name="keywords" content="wirtaw,poplauki,vladimir poplavskij,php-programuotojas,žaidimai,programavimas" />';
                        $meta['Author']='<meta name="author" content="Vladimir Poplavskij" />';
                        //$meta['Language']='<meta http-equiv="content-language" content="lt" />';
                        $meta['Robots']='<meta name="robots" content="all" />';
                        $meta['Canonical']='<link rel="canonical" href="#" />';
                    break;
                case 'pl':
                        $meta['Content-Type']='<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
                        //$meta['Date']='<meta http-equiv="date" content="2011-07-16T22:22:22+00:00" />';
                        //$meta['Last-Modified']='<meta http-equiv="Last-Modified" content="2014-02-16T09:10:56+00:00" />';
                        $meta['Description']='<meta name="description" content="Strona domowa Wladzimierza Poplawskiego" />';
                        $meta['Keywords']='<meta name="keywords" content="wirtaw,poplauki,poplavskij,php developer,programowanie" />';
                        $meta['Author']='<meta name="author" content="Vladimir Poplavskij" />';
                        //$meta['Language']='<meta http-equiv="content-language" content="pl" />';
                        $meta['Robots']='<meta name="robots" content="all" />';
                        $meta['Canonical']='<link rel="canonical" href="#" />';
                    break;
                }
                $meta['Generator']='<meta name="generator" content="Geany 0.18,Bluefish 1.0.7,NetBeans 7.2" />';
                
                
                $meta['google']='<meta name="google-site-verification" content="c53AFW0Ddz53kE2lf9MLqcBVq7cIBZ74tuWGceVOvuw" />';
                $meta['msvalidate']='<meta name="msvalidate.01" content="A2F041987677644A93B603E6A7E80B49" />';
                //$meta['screenshot']='<link rel="screenshot" type="image/png" title="poplauki.eu" href="http://poplauki.eu/images/screenshot.png" />';
                $meta['favicon']='<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">';
		foreach($meta as $value){ 
                    $str.= $value."\n";
                    
                }
	return $str;
}

function Add_Counter_Metrics($show){
	if($show){
		return '<!-- Yandex.Metrika counter --><script type="text/javascript">var yaParams = {/*Здесь параметры визита*/};</script><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter279643 = new Ya.Metrika({id:279643, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true,params:window.yaParams||{ }}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/279643" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->';
	}else{
		return "";
	}
	
}
function Add_Google_Tag_Manager($show){ 
    if($show){
		return '<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7L6JT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
\'//www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,\'script\',\'dataLayer\',\'GTM-N7L6JT\');</script>
<!-- End Google Tag Manager -->
';
    }else{
            return "";
    }
}
/* 
Add new entry
<?php if(isset($_SESSION['visitor']['lang'])){echo '';}else{echo '';} ?> 

*/
?>
