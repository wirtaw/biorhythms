<?php
error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
$font = 'fonts/FreeSans.ttf';
$dayd=date('j');
$monthd=date('n');
$yeard=date('Y');
$size = 5;
$r = 255;
$g = 255;
$b = 255;
$diagramWidth = 540;
$diagramHeight = 480;  
$daysToShow=40;
$mas_lang=array();
if(isset($_GET['month'])){$birthMonth = intval($_GET['month']);}
if(isset($_GET['day'])){$birthDay = intval($_GET['day']);}
if(isset($_GET['year'])){$birthYear = intval($_GET['year']);}
if(isset($_GET['name'])){$username = strip_tags(trim($_GET['name']));}
if(isset($_GET['id'])){$name_id = intval($_GET['id']);}
if(isset($_POST['datepicker'])) {
    $datepicker = trim($_POST['datepicker']);
    if (isset($_POST['old_lang'])) {
        $old_lang = $_POST['old_lang'];
        if( strcmp($old_lang,'ru') === 0 ) {
            $mas_date = explode('.',$datepicker);
            if( strcmp($_SESSION['visitor']['lang'],'en') === 0 ) {
                $datepicker  =  ''.$mas_date[1].'/'.$mas_date[0].'/'.$mas_date[2];
            } elseif( strcmp($_SESSION['visitor']['lang'],'lt') === 0 ) {
                $datepicker  =  ''.$mas_date[2].'-'.$mas_date[1].'-'.$mas_date[0];
            }
            $birthYear = $mas_date[2]; 
            $birthMonth = $mas_date[1];
            $birthDay = $mas_date[0];
        } elseif( strcmp($old_lang,'en') === 0 ) {
            $mas_date = explode('/',$datepicker);
            if( strcmp($_SESSION['visitor']['lang'],'ru') === 0 ) {
                $datepicker  =  ''.$mas_date[1].'.'.$mas_date[0].'.'.$mas_date[2];
            } elseif( strcmp($_SESSION['visitor']['lang'],'pl') === 0 ) {
                $datepicker  =  ''.$mas_date[1].'.'.$mas_date[0].'.'.$mas_date[2];
            } elseif( strcmp($_SESSION['visitor']['lang'],'lt') === 0 ) {
                $datepicker  =  ''.$mas_date[2].'-'.$mas_date[0].'-'.$mas_date[1];
            }
            $birthYear = $mas_date[2]; 
            $birthMonth = $mas_date[1];
            $birthDay = $mas_date[0];
        } elseif( strcmp($old_lang,'pl') === 0 ) {
            $mas_date = explode('.',$datepicker);
            if( strcmp($_SESSION['visitor']['lang'],'en') === 0 ) {
                $datepicker  =  ''.$mas_date[1].'/'.$mas_date[0].'/'.$mas_date[2];
            } elseif( strcmp($_SESSION['visitor']['lang'],'lt') === 0 ) {
                $datepicker  =  ''.$mas_date[2].'-'.$mas_date[1].'-'.$mas_date[0];
            }
            $birthYear = $mas_date[2]; 
            $birthMonth = $mas_date[1];
            $birthDay = $mas_date[0];
        } elseif( strcmp($old_lang,'lt') === 0 ) {
            $mas_date = explode('-',$datepicker);
            if( strcmp($_SESSION['visitor']['lang'],'ru') === 0 ) {
                $datepicker  =  ''.$mas_date[2].'.'.$mas_date[1].'.'.$mas_date[0];
            } elseif( strcmp($_SESSION['visitor']['lang'],'en') === 0 ) {
                $datepicker  =  ''.$mas_date[1].'/'.$mas_date[2].'/'.$mas_date[0];
            } elseif( strcmp($_SESSION['visitor']['lang'],'pl') === 0 ) {
                $datepicker  =  ''.$mas_date[2].'.'.$mas_date[1].'.'.$mas_date[0];
            }
            $birthYear = $mas_date[0]; 
            $birthMonth = $mas_date[1];
            $birthDay = $mas_date[2];
        }
        
    }
} else {
    if (isset($birthMonth) && isset($birthDay) && isset($birthYear)) {
        if (true === checkdate( $birthMonth,$birthDay,$birthYear)) {
            if( strcmp($_SESSION['visitor']['lang'],'ru') === 0 ) {
                $datepicker  =  ''.$birthDay.'.'.$birthMonth.'.'.$birthYear;
            } elseif( strcmp($_SESSION['visitor']['lang'],'en') === 0 ) {
                $datepicker  =  ''.$birthDay.'/'.$birthMonth.'/'.$birthYear;
            } elseif( strcmp($_SESSION['visitor']['lang'],'pl') === 0 ) {
                $datepicker  =  ''.$birthDay.'.'.$birthMonth.'.'.$birthYear;
            } elseif( strcmp($_SESSION['visitor']['lang'],'lt') === 0 ) {
                $datepicker  =  ''.$birthYear.'-'.$birthMonth.'-'.$birthDay;
            }
        } else {
            $birthYear = 1977; 
            $birthMonth = 10;
            $birthDay = 12;
        }
    } else {
       $birthYear = 1977; 
       $birthMonth = 10;
       $birthDay = 12;
    }
}
if(isset($_POST['textinput'])) {
    $username = trim($_POST['textinput']);
}
$lang='ru';
if(!isset($_SESSION['visitor']['lang'])){$_SESSION['visitor']['lang']=$lang;}
$mas_lang['keyword']='Время жизни,дни потраченные на сон,еду,учёбу,работу,круглая дата,дни жизни,биоритмы, интелектуальный, ритмы, физический, эмоциональный,kiek pralesta dienu miegant,valgant,besimokant, sportojant,jubelejines datos,gynenimo dienos,bioritmai, inteketualinis ritmas,fizinis ritmas,emocinis ritmas,bioritmo progonozė,how much days spent to sleep,food,sport,study,celbrate dates,life days,biorythms, intelektual rythm,physical rythm,emocional rythm,ile spedzal czasu spac,Ile spedzal czasu jedzac,oblic biorytm ,moj biorytm,biorythm';
/*$mas_lang['langbar']='<img src="images/gb.png"  width="16" height="11" onclick="javascript:select_lang(\'en\');" class="lng" border="0"> <img src="images/lt.png" onclick="javascript:select_lang(\'lt\');" class="lng"  width="16" height="11" border="0"> <img src="images/ru.png" onclick="javascript:select_lang(\'ru\');" class="lng"  width="16" height="11" border="0"> <img src="images/pl.png" onclick="javascript:select_lang(\'pl\');" class="lng"  width="16" height="11" border="0">';*/
//$file_name=''.$_SESSION['visitor']['lang'].'.json';
$file_name='conf/'.$_SESSION['visitor']['lang'].'.json';
switch($_SESSION['visitor']['lang']) {
      case 'ru':
          //$file_name=$_SESSION['visitor']['lang'].'.json';
      break;
      case 'lt':
	//$file_name='';	
      break;
      case 'en':
	//$file_name='';	
      break;
      case 'pl':
	//$file_name='';	
      break;
}
if(file_exists($file_name)) {
    $source=@file_get_contents($file_name);
    if(!empty($source)) {
        $mas_source=json_decode($source);
        //var_dump($mas_source);
        if(isset($mas_source->text) && is_object($mas_source->text)){
            $text=array();
            foreach($mas_source->text as $key => $value){
                $text[$key]=$value;
            }
        }
        if(isset($mas_source->montharray) && is_object($mas_source->montharray)){
            $motharray=array();
            //$motharray=array_merge($motharray,$mas_source->motharray);
            foreach($mas_source->montharray as $key => $value){
                $motharray[$key]=$value;
            }
        }
        if(isset($mas_source->mas_lang) && is_object($mas_source->mas_lang)){
            foreach($mas_source->mas_lang as $key => $value){
                $mas_lang[$key]=str_replace(array('&amp;','&cuot;','&rslash;'),array('&','"','/'),$value);
                //echo "\n $key =>  $value\n";
            }
        }
        if(isset($mas_source->birth_number_text) && is_object($mas_source->birth_number_text)){
            $birth_number_text=array();
            foreach($mas_source->birth_number_text as $key => $value){
                $birth_number_text[$key]=str_replace(array('&amp;','&cuot;','&rslash;'),array('&','"','/'),$value);
                //echo "\n $key =>  $value\n";
            }
        }
        //var_dump($mas_lang);
    }
}

if(!isset($_SESSION['count'])) {
    $_SESSION['count']=0;
}
//echo var_dump($mas_lang);
?>
