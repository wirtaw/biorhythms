<?php
function parseFloat($ptString) {
            if (strlen($ptString) == 0) {
                    return false;
            }
           
            $pString = str_replace(" ", "", $ptString);
           
            if (substr_count($pString, ",") > 1)
                $pString = str_replace(",", "", $pString);
           
            if (substr_count($pString, ".") > 1)
                $pString = str_replace(".", "", $pString);
           
            $pregResult = array();
       
            $commaset = strpos($pString,',');
            if ($commaset === false) {$commaset = -1;}
       
            $pointset = strpos($pString,'.');
            if ($pointset === false) {$pointset = -1;}
       
            $pregResultA = array();
            $pregResultB = array();
       
            if ($pointset < $commaset) {
                preg_match('#(([-]?[0-9]+(\.[0-9])?)+(,[0-9]+)?)#', $pString, $pregResultA);
            }
            preg_match('#(([-]?[0-9]+(,[0-9])?)+(\.[0-9]+)?)#', $pString, $pregResultB);
            if ((isset($pregResultA[0]) && (!isset($pregResultB[0])
                    || strstr($preResultA[0],$pregResultB[0]) == 0
                    || !$pointset))) {
                $numberString = $pregResultA[0];
                $numberString = str_replace('.','',$numberString);
                $numberString = str_replace(',','.',$numberString);
            }
            elseif (isset($pregResultB[0]) && (!isset($pregResultA[0])
                    || strstr($pregResultB[0],$preResultA[0]) == 0
                    || !$commaset)) {
                $numberString = $pregResultB[0];
                $numberString = str_replace(',','',$numberString);
            }
            else {
                return false;
            }
            $result = (float)$numberString;
            return $result;
} 
function TakeDates($days,$div)
{
      $mas = array();
      $i=$days;
      while($i<42000||count($mas)<11)
      {
	    if($i%$div==0)
	    {
		$mas[]=$i;  
	    }
	    $i++;
      }
      return $mas;
}
function DayLeft_Date($cdate,$days)
{
      return strtotime("+".($cdate-$days)." day");
}
function win_uni($in){
  $in = convert_cyr_string($in ,"w","i");
  $out = "";
  for ($i=0; $i < strlen($in); $i++) {
    $char = ord($in[$i]);
    $out .= ($char > 175)?"&#".(1040+($char-176)).";":$in[$i];
  }
  return $out;
}
function unicode_russian($str) {
     $encode = "";
     for ($ii=0;$ii<strlen($str);$ii++) {
         $xchr=substr($str,$ii,1);
         if (ord($xchr)>191) {
             $xchr=ord($xchr)+848;
             $xchr="&#" . $xchr . ";";
         }
         if(ord($xchr) == 168) {
               $xchr = "&#1025;"; 
         }
         if(ord($xchr) == 184) {
               $xchr = "&#1105;"; 
         }
         $encode=$encode . $xchr;
   }
   return $encode;
}
function gregorianToJD2($month, $day, $year){
    if($month < 3){
        $month = $month + 12;
        $year = $year - 1;
    }
    $jd = $day + floor((153 * $month - 457) / 5) + 365 * $year + floor($year / 4) - floor($year / 100) + floor($year / 400) + 1721118.5;
    return $jd;
}
function drawRhythm($daysAlive, $period, $color)
{
    global $daysToShow, $im, $diagramWidth, $diagramHeight;
    $centerDay = $daysAlive - ($daysToShow / 2);
    $plotScale = ($diagramHeight - 25) / 2;
    $plotCenter = ($diagramHeight - 25) / 2;
    for($x = 0; $x <= $daysToShow; $x++)
    {
        $phase = (($centerDay + $x) % $period) / $period * 2 * pi();
        $y = 1 - sin($phase) * (float)$plotScale + (float)$plotCenter;
        if($x > 0)imageLine($im, $oldX, $oldY, $x * $diagramWidth / $daysToShow,$y, $color);
        $oldX = $x * $diagramWidth / $daysToShow;
        $oldY = $y;
    }
}
function drawRhythm_2($daysAlive, $period, $color)
{
    global $daysToShow, $im, $diagramWidth, $diagramHeight;
    
    $centerDay = $daysAlive - ($daysToShow / 2);
    $plotScale = (($diagramHeight - 25) / 2)+2;
    $plotCenter = (($diagramHeight - 25) / 2)+22;
    for($x = 0; $x <= ($daysToShow-1); $x++)
    {
        $phase = (($centerDay + $x) % $period) / $period * 2 * pi();
        $y = 1 - sin($phase) * (float)$plotScale + (float)$plotCenter;
        if($x > 0 && $x < ($daysToShow-1)){
            imageLine($im, $oldX+20, $oldY, $x * ($diagramWidth+20)  / $daysToShow+20,$y, $color);
        }elseif($x == ($daysToShow-1)){
            imageLine($im, $oldX+20, $oldY, $x * ($diagramWidth+20)  / $daysToShow+20,$y, $color);
        }
        $oldX = $x * ($diagramWidth+20) / $daysToShow;
        $oldY = $y;
    }
}
function drawRhythm_3($daysAlive, $color)
{
    global $daysToShow, $im, $diagramWidth, $diagramHeight;
    $centerDay = $daysAlive - ($daysToShow / 2);
    $plotScale = ($diagramHeight - 25) / 2+2;
    $plotCenter = ($diagramHeight - 25) / 2+22;
    $a=22;
	$b=28;
	$c=33;
	for($x = 0; $x <= $daysToShow-1; $x++)
    {
        $phase1 = (($centerDay + $x) % $a) / $a * 2 * pi();
		$phase2 = (($centerDay + $x) % $b) / $b * 2 * pi();
		$phase3 = (($centerDay + $x) % $c) / $c * 2 * pi();
		//$phase=($phase1 +$phase2 +$phase3)/3;
        $y1 = 1 - sin($phase1) * (float)$plotScale + (float)$plotCenter;
		$y2 = 1 - sin($phase2) * (float)$plotScale + (float)$plotCenter;
		$y3 = 1 - sin($phase3) * (float)$plotScale + (float)$plotCenter;
		$y = ($y1+$y2+$y3)/3;
        if($x > 0)imageLine($im, $oldX+20, $oldY, $x * ($diagramWidth+20) / $daysToShow+20,$y, $color);
        //if($x > 0)imageSetPixel($im,$x,$y, $color);
        $oldX = $x * ($diagramWidth+20) / $daysToShow;
        $oldY = $y;
    }
}
function drawRhythm3($daysAlive, $color)
{
    global $daysToShow, $im, $diagramWidth, $diagramHeight;
    $centerDay = $daysAlive - ($daysToShow / 2);
    $plotScale = ($diagramHeight - 25) / 2;
    $plotCenter = ($diagramHeight - 25) / 2;
    $a=22;
	$b=28;
	$c=33;
	for($x = 0; $x <= $daysToShow; $x++)
    {
        $phase1 = (($centerDay + $x) % $a) / $a * 2 * pi();
		$phase2 = (($centerDay + $x) % $b) / $b * 2 * pi();
		$phase3 = (($centerDay + $x) % $c) / $c * 2 * pi();
		//$phase=($phase1 +$phase2 +$phase3)/3;
        $y1 = 1 - sin($phase1) * (float)$plotScale + (float)$plotCenter;
		$y2 = 1 - sin($phase2) * (float)$plotScale + (float)$plotCenter;
		$y3 = 1 - sin($phase3) * (float)$plotScale + (float)$plotCenter;
		$y = ($y1+$y2+$y3)/3;
        if($x > 0)imageLine($im, $oldX, $oldY, $x * $diagramWidth / $daysToShow,$y, $color);
        $oldX = $x * $diagramWidth / $daysToShow;
        $oldY = $y;
    }
}
function drawRhythm4($daysAlive)
{
    global $daysToShow, $im, $diagramWidth, $diagramHeight;
    $centerDay = $daysAlive ;
    $plotScale = ($diagramHeight - 25) / 2;
    $plotCenter = ($diagramHeight - 25) / 2;
    $a=22;
	$b=28;
	$c=33;
	$x=$centerDay;
    $phase1 = ($x % $a) / $a * 2 * pi();
	$phase2 = ($x % $b) / $b * 2 * pi();
	$phase3 = ($x % $c) / $c * 2 * pi();
    $y1 = sin($phase1);
	$y2 = sin($phase2);
	$y3 = sin($phase3);
	$y = ($y1+$y2+$y3)/3;
	$holla = round($y,2);
	return str_replace(".",",",$holla);
}

function drawRhythm2($daysAlive, $period)
{
    global $daysToShow, $im, $diagramWidth, $diagramHeight;
    $centerDay = $daysAlive ;
    $plotScale = ($diagramHeight - 25) / 2;
    $plotCenter = ($diagramHeight - 25) / 2;
	$x=$centerDay;
    $phase = ($x % $period) / $period * 2 * pi();
    $y = sin($phase);// +(float)$plotScale + (float)$plotCenter ;
	//imageLine($im, $x * $diagramWidth / $daysToShow, $y, $x * $diagramWidth / $daysToShow,$y, $color);
	/*$proz = 100;
	$y = $y * (float)$proz; // + (float)$plotCenter;*/
	$holla = round($y,2);
	return str_replace(".",",",$holla);
}
function count_days($dd,$mm,$yy) {
			$den=0;
			$pg=$yy-1900; 
			$vis=floor($pg/4); 
			$pg-=$vis; 
			$den=($vis*366)+($pg*365); 
			for($i=1;$i<$mm;$i++) {
				if (($i==1)||($i==3)||($i==5)||($i==7)||($i==8)||($i==10)||($i==12)){ $den+=31; }
				if (($i==4)||($i==6)||($i==9)||($i==11)){ $den+=30; }
				if (($i==2)&&(!ereg(".",$yy/4))){ $den+=29; } 
				if (($i==2)&&(ereg(".",$yy/4))){ $den+=28; } 
			}
			$den+=$dd; 
			return $den;
}
function name_id(){
	$files=array('js/a.dat','js/b.dat','js/ce.dat','js/d.dat','js/f.dat','js/h.dat','js/g.dat','js/i.dat','js/ia.dat','js/iu.dat','js/k.dat','js/l.dat','js/m.dat','js/n.dat','js/o.dat','js/p.dat','js/s.dat','js/r.dat','js/t.dat','js/u.dat','js/v.dat','js/z.dat','js/zh.dat');
	$my_name=$_SESSION['name'];
	if(isset($_SESSION['id'])){$id=$_SESSION['id'];}else{$id=-1;}
	$counter=array();
	$mas=array();
	for($i=0;$i<count($files);$i++){
		$filename=$files[$i];
		if(file_exists($filename)){
			$counter[$i]=0;
			$rf=file($filename);
			$len=count($rf);
			for($j=0;$j<$len;$j++){
				$mas[]=$rf[$j];
				$counter[$i]++;
			}
		}
	}
	if(isset($mas)){
		$len=count($mas);
		for($j=0;$j<$len;$j++){
			if(strstr($mas[$j],'<span class=name>'.$my_name.'</span>')){
				$id=$j;
			}
		}
	}
	return $id;
}
function id_by_name($my_id){
	$files=array('js/a.dat','js/b.dat','js/ce.dat','js/d.dat','js/f.dat','js/h.dat','js/g.dat','js/i.dat','js/ia.dat','js/iu.dat','js/k.dat','js/l.dat','js/m.dat','js/n.dat','js/o.dat','js/p.dat','js/s.dat','js/r.dat','js/t.dat','js/u.dat','js/v.dat','js/z.dat','js/zh.dat');
	$my_name='';
	$id=$my_id;
	$counter=array();
	$mas=array();
	for($i=0;$i<count($files);$i++){
		$filename=$files[$i];
		if(file_exists($filename)){
			$counter[$i]=0;
			$rf=file($filename);
			$len=count($rf);
			for($j=0;$j<$len;$j++){
				$mas[]=$rf[$j];
				$counter[$i]++;
			}
		}
	}
	if(isset($mas)){
		$len=count($mas);
		for($j=0;$j<$len;$j++){
			if($id==$j){
				$my_name=$mas[$j];
			}
		}
	}
	return $my_name;
}
function leta($in){
 if(isset($_SESSION['lang'])){$lang=$_SESSION['lang'];}else{$lang='ru';}
 switch ($lang){
      case 'ru':
	if($in%10==1){
		return 'год';
	}elseif($in%10==2){
		return 'год';
	}elseif($in%10==3){
		return 'года';
	}elseif($in%10==4){
		return 'года';
	}elseif($in%10==5){
		return 'лет';
	}elseif($in%10==6){
		return 'лет';
	}elseif($in%10==7){
		return 'лет';
	}elseif($in%10==8){
		return 'лет';
	}elseif($in%10==9){
		return 'лет';
	}elseif($in%10==0){
		return 'лет';
	}elseif($in>9&&$in<20){
		return 'лет';
	}
      break;
      case 'lt':
	if($in%10==1){
		return 'metai';
	}elseif($in%10==2){
		return 'metai';
	}elseif($in%10==3){
		return 'metai';
	}elseif($in%10==4){
		return 'metai';
	}elseif($in%10==5){
		return 'metai';
	}elseif($in%10==6){
		return 'metai';
	}elseif($in%10==7){
		return 'metai';
	}elseif($in%10==8){
		return 'metai';
	}elseif($in%10==9){
		return 'metai';
	}elseif($in%10==0){
		return 'metų';
	}elseif($in>9&&$in<20){
		return 'metų';
	}
      break;
      case 'pl':
	if($in%10==1){
		return 'god';
	}elseif($in%10==2){
		return 'lata';
	}elseif($in%10==3){
		return 'lata';
	}elseif($in%10==4){
		return 'lat';
	}elseif($in%10==5){
		return 'lat';
	}elseif($in%10==6){
		return 'lat';
	}elseif($in%10==7){
		return 'lat';
	}elseif($in%10==8){
		return 'lat';
	}elseif($in%10==9){
		return 'lat';
	}elseif($in%10==0){
		return 'lat';
	}elseif($in>9&&$in<20){
		return 'lat';
	}
      break;
      case 'en':
	return 'years';
      break;
 }
}
function dni($in,$lang){
        switch ($lang){
              case 'ru':
                if($in%10==1){
                        return 'день';
                }elseif($in==1){
                        return 'день';
                }elseif($in==2&&$in==3&&$in==4){
                        return 'дня';
                }elseif($in%10>9&&$in%10<20){
                        return 'дней';
                }elseif($in>9&&$in<20){
                        return 'дней';
                }elseif($in%10==2){
                        return 'дня';
                }elseif($in%10==3){
                        return 'дня';
                }elseif($in%10==4){
                        return 'дня';
                }elseif($in%10==5){
                        return 'дней';
                }elseif($in%10==6){
                        return 'дней';
                }elseif($in%10==7){
                        return 'дней';
                }elseif($in%10==8){
                        return 'дней';
                }elseif($in%10==9){
                        return 'дней';
                }else{
                        return 'дней';
                }
              break;
              case 'lt':
                if($in%10==1){
                        return 'dienų';
                }elseif($in==1){
                        return 'dieną';
                }elseif($in==2&&$in==3&&$in==4){
                        return 'dienos';
                }elseif($in%10>9&&$in%10<20){
                        return 'dienų';
                }elseif($in>9&&$in<20){
                        return 'dienų';
                }elseif($in%10==2){
                        return 'dienos';
                }elseif($in%10==3){
                        return 'dienos';
                }elseif($in%10==4){
                        return 'dienos';
                }elseif($in%10==5){
                        return 'dienos';
                }elseif($in%10==6){
                        return 'dienos';
                }elseif($in%10==7){
                        return 'dienos';
                }elseif($in%10==8){
                        return 'dienos';
                }elseif($in%10==9){
                        return 'dienos';
                }else{
                        return 'dienos';
                }  
              break;
              case 'pl':
                if($in%10==1){
                        return 'dzien';
                }elseif($in==1){
                        return 'dzien';
                }elseif($in==2&&$in==3&&$in==4){
                        return 'dni';
                }elseif($in%10>9&&$in%10<20){
                        return 'dni';
                }elseif($in>9&&$in<20){
                        return 'dni';
                }elseif($in%10==2){
                        return 'dni';
                }elseif($in%10==3){
                        return 'dni';
                }elseif($in%10==4){
                        return 'dni';
                }elseif($in%10==5){
                        return 'dni';
                }elseif($in%10==6){
                        return 'dni';
                }elseif($in%10==7){
                        return 'dni';
                }elseif($in%10==8){
                        return 'dni';
                }elseif($in%10==9){
                        return 'dni';
                }else{
                        return 'dni';
                }  
              break;
              case 'en':
                if($in%10==1){
                        return 'days';
                }elseif($in==1){
                        return 'day';
                }elseif($in==2&&$in==3&&$in==4){
                        return 'days';
                }elseif($in%10>9&&$in%10<20){
                        return 'days';
                }elseif($in>9&&$in<20){
                        return 'days';
                }elseif($in%10==2){
                        return 'days';
                }elseif($in%10==3){
                        return 'days';
                }elseif($in%10==4){
                        return 'days';
                }elseif($in%10==5){
                        return 'days';
                }elseif($in%10==6){
                        return 'days';
                }elseif($in%10==7){
                        return 'days';
                }elseif($in%10==8){
                        return 'days';
                }elseif($in%10==9){
                        return 'days';
                }else{
                        return 'days';
                } 
              break;
              }
        }

function GetPlus($source,$x,$id){
	$phase=round(sin((($source+$x)%$id)/$id*2*pi()),3);
	//$d=$phase;
	return $phase;
}
function procT($x,$y){
	return floor(($y/$x)*100);
}
function predrod($x,$id){
	return $x%$id;
}
function my_random($min,$max){
	$a=$min;
	$b=$max;
	$x=$max-$min;
	$sum=0;
	$dif=0;
	for($i=$a;$i<$b;$i++){
		$sum*=rand(0,$i);
		$dif+=rand(0,$i);
	}
	return $number=floor(round($sum%$x)+round($dif%$x)); 
}

function random_int_float($min,$max) {
	    return round(($min+lcg_value()*(abs($max-$min))));
}

function show_table_years($years,$days){
	if(isset($_SESSION['lang'])&&$_SESSION['lang']=='ru'){
		$mas_lang['part_daylife']=array('Сон','Учёба','Работа ','Еда','Свободное время','Спорт','Чистили зубы','Ждали зелёный свет светофора','Выплакано слёз','Учёба  до 25 лет','Работа до 63 лет','Спорт до 63 лет');
		$lang='ru';
		$mas_lang['s']=' Из ';
	}elseif(isset($_SESSION['lang'])&&$_SESSION['lang']=='en'){
		$mas_lang['part_daylife']=array('Sleep','Education','Work','Food','Free time','Sport','Brush you teeth','Waited green light','Cry tears ','Education  up to 25 years','Work up to  63 years','Sport up to  63 years');
		$mas_lang['days_wr']=' days';
		$lang='en';
		$mas_lang['years_wr']=' years';
		$mas_lang['s']=' From ';
		$mas_lang['l']='l';
	}elseif(isset($_SESSION['lang'])&&$_SESSION['lang']=='lt'){
		$mas_lang['part_daylife']=array('Miegas ','Mokslas ','Darbas ','Valgymas ','Laisvalaikis ','Sportas ','Dantims valyti ','Laukete žalios šviesoforo šviesos ','Nuverkta ašaru ','Mokslai iki 25 metų','Darbas iki 63 metų','Sportas iki 63 metų');
		$mas_lang['days_wr']=' dienos';
		$mas_lang['years_wr']=' metu';
		$lang='lt';
		$mas_lang['s']=' Iš ';
		$mas_lang['l']='l';
	}elseif(isset($_SESSION['lang'])&&$_SESSION['lang']=='pl'){
		$mas_lang['part_daylife']=array('Sen','Nauka ','Praca ','Posilki ','Wolny czas ','Sport ','Myć zęby','Czekając zielone światło','łzy płacz','Nauka do 25 łat','Praca do 63 łat','Sport do 63 łat');
		$mas_lang['days_wr']=' dni';
		$mas_lang['years_wr']=' łat';
		$mas_lang['s']=' Z ';
		$lang='pl';
		$mas_lang['l']='l';
	}else{
		$mas_lang['part_daylife']=array('Сон','Учёба','Работа ','Еда','Свободное время','Спорт','Чистили зубы','Ждали зелёный свет светофора','Выплакано слёз','Учёба  до 25 лет','Работа до 63 лет','Спорт до 63 лет');
		$lang='ru';
	}
	if($lang=='ru'){$table='<table height="400" width="500" class="posit"><tr><td><span style="text-align:center;">Из <span id="red">'.$days.'</span> '.dni($days).'( <span id="red">'.$years.' '.leta($years).'</span>) :</span></td><td>дней</td><tr>';}else{
	$table='<table height="400" width="500" class="posit"><tr><td><span style="text-align:center;"> '.$mas_lang['s'].'<span id="red">'.$days.'</span> '.$mas_lang['days_wr'].'( <span id="red">'.$years.' '.$mas_lang['years_wr'].'</span>) :</span></td><td>'.$mas_lang['days_wr'].'</td><tr>';}
	if($years>0&&$years<=25){
			$son=floor(($days*18)/48);
			$uceba=floor(($days*9)/48);
			$eda=floor(($days*5)/48);
			$free=floor(($days*14)/48);
			$tren=floor(($days*1)/48);
			$slezy=floor(($days*25)/9131);
			$zel=floor(($days*61.25)/9131);
			$zuby=floor(($days*15)/9131);
			if($lang=='ru'){	
				$table.='<td width="200" class="drs">Сон </td><td class="drs">'.$son.' '.dni($son).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Учёба </td><td class="drs">'.$uceba.' '.dni($uceba).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Еда </td><td class="drs">'.$eda.' '.dni($eda).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Свободное время </td><td class="drs">'.$free.' '.dni($free).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Спорт </td><td class="drs">'.$tren.' '.dni($tren).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Чистили зубы </td><td class="drs">'.$zuby.' '.dni($zuby).'</td></tr>';
				$table.='<tr><td width="200" class="drs">Ждали зелёный свет светофора</td><td class="drs">'.$zel.' '.dni($zel).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Выплакано слёз </td><td class="drs">'.$slezy.' л </td></tr>';
			}else{
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][0].' </td><td class="drs">'.$son.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][1].' </td><td class="drs">'.$uceba.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][3].' </td><td class="drs">'.$eda.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][4].' </td><td class="drs">'.$free.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][5].' </td><td class="drs">'.$tren.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][6].' </td><td class="drs">'.$zuby.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][7].' </td><td class="drs">'.$zel.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][8].' </td><td class="drs">'.$slezy.' '.$mas_lang['l'].' </td></tr>';
			}
		}elseif($years>25&&$years<=63){
			$days_y=$days-9131;
			$uceba_y=floor((9131*9)/48);
			$son=floor(($days_y*16)/48)+floor((9131*18)/48);
			$rabota=floor(($days_y*20)/48);
			$eda=floor(($days_y*5)/48)+floor((9131*5)/48);
			$free=floor(($days_y*6)/48)+floor((9131*14)/48);
			$tren=floor(($days_y*1)/48)+floor((9131*1)/48);
			$slezy=25+floor(($days_y*40)/$days);
			$zel=61+floor(($days_y*64.25)/$days);
			$zuby=15+floor(($days_y*14)/$days);
			//echo ' свободное y: '.$days_y.' m : '.$days.' z1 '.floor(($days*15)/9131).' '.floor(($days_y*14)/$days).'<br>';
			if($lang=='ru'){
				$table.='<td width="200" class="drs">Сон </td><td class="drs">'.$son.' '.dni($son).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Работа </td><td class="drs">'.$rabota.' '.dni($rabota).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Еда </td><td class="drs">'.$eda.' '.dni($eda).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Свободное время </td><td class="drs">'.$free.' '.dni($free).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Спорт </td><td class="drs">'.$tren.' '.dni($tren).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Учёба  до 25 лет</td><td class="drs">'.$uceba_y.' '.dni($uceba_y).'</td></tr>';
				$table.='<tr><td width="200" class="drs">Чистили зубы </td><td class="drs">'.$zuby.' '.dni($zuby).'</td></tr>';
				$table.='<tr><td width="200" class="drs">Ждали зелёный свет светофора</td><td class="drs">'.$zel.' '.dni($zel).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Выплакано слёз </td><td class="drs">'.$slezy.' л </td></tr>';
			}else{
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][0].' </td><td class="drs">'.$son.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][2].' </td><td class="drs">'.$son.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][3].' </td><td class="drs">'.$eda.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][4].' </td><td class="drs">'.$free.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][5].' </td><td class="drs">'.$tren.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][9].' </td><td class="drs">'.$tren.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][6].' </td><td class="drs">'.$zuby.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][7].' </td><td class="drs">'.$zel.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][8].' </td><td class="drs">'.$slezy.' '.$mas_lang['l'].' </td></tr>';
			}
		}elseif($years>63){
			$days_y=9131;
			$days_o=23058;
			$days_m=$days-$days_o;
			$days_t=36526;
			//echo 'y: '.$days_y.' m : '.$days_m.' o: '.$days_o.'<br>';
			//echo ' сон y: '.$son_y.' m : '.$son_m.' o: '.$son_o.'<br>';
			//echo ' еда y: '.$eda_y.' m : '.$eda_m.' o: '.$eda_o.'<br>';
			//echo ' свободное y: '.$free_y.' m : '.$free_m.' o: '.$free_o.'<br>';
			$tren=floor(($days_m*1)/48)+floor(($days_y*1)/48);
			//echo ' спорт y: '.$tren_y.' m : '.$tren_m.' <br>';
			$uceba=floor(($days_y*9)/48);
			//echo ' учёба y: '.$uceba.'<br>';
			$son=floor(($days_y*18)/48)+floor(($days_m*16)/48)+floor(($days_y*18)/48);
			$rabota=floor(($days_m*20)/48);
			$eda=floor(($days_y*4)/48)+floor(($days_m*5)/48)+floor(($days_y*5)/48);
			$free=floor(($days_y*26)/48)+floor(($days_m*6)/48)+floor(($days_y*14)/48);
			$slezy=65+floor(($days*5)/$days_t);
			$zel=66+floor(($days*57)/$days_t);
			$zuby=29+floor(($days*7)/$days_t);
			//echo ' свободное y: '.$days_y.' m : '.$days_m.' o: '.$days_o.' s: '.$days.'<br>';
			if($lang=='ru'){
				$table.='<td width="200" class="drs">Сон </td><td class="drs">'.$son.' '.dni($son).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Еда </td><td class="drs">'.$eda.' '.dni($eda).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Свободное время </td><td class="drs">'.$free.' '.dni($free).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Работа до 63 лет </td><td class="drs">'.$rabota.' '.dni($rabota).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Спорт до 63 лет </td><td class="drs">'.$tren.' '.dni($tren).' </tr>';
				$table.='<tr><td width="200" class="drs">Учёба до 25 лет </td><td class="drs">'.$uceba.' '.dni($uceba).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Чистили зубы </td><td class="drs">'.$zuby.' '.dni($zuby).'</td></tr>';
				$table.='<tr><td width="200" class="drs">Ждали зелёный свет светофора</td><td class="drs">'.$zel.' '.dni($zel).' </td></tr>';
				$table.='<tr><td width="200" class="drs">Выплакано слёз </td><td class="drs">'.$slezy.' л </td></tr>';
			}else{
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][0].' </td><td class="drs">'.$son.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][3].' </td><td class="drs">'.$eda.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][4].' </td><td class="drs">'.$free.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][10].' </td><td class="drs">'.$rabota.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][11].' </td><td class="drs">'.$tren.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][9].' </td><td class="drs">'.$tren.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][6].' </td><td class="drs">'.$zuby.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][7].' </td><td class="drs">'.$zel.' '.$mas_lang['days_wr'].' </td></tr>';
				$table.='<td width="200" class="drs">'.$mas_lang['part_daylife'][8].' </td><td class="drs">'.$slezy.' '.$mas_lang['l'].' </td></tr>';
			}
		}
		$table.='</tr></table>';
		return $table;
}
?>
