<?php
	if(isset($_GET['lng'])) {
	    $lng = htmlentities(trim($_GET['lng']));
            if(!in_array($lng,array('ru','en','lt','pl'))){ unset($lng); }
	}
	
        if(isset($_GET['r'])){$r=intval($_GET['r']);}
        if(isset($_GET['y'])){$y=intval($_GET['y']);}
        if(isset($_GET['m'])){$m=intval($_GET['m']);}
        if(isset($_GET['d'])){$d=intval($_GET['d']);}
	if(isset($y)&&isset($m)&&isset($d)&&checkdate($m,$d,$y)) {
            $days=abs(gregorianToJD2($m, $d, $y)-gregorianToJD2(date("m"),date("d"),date("Y")));
        }
	function gregorianToJD2($month, $day, $year) {
		if($month < 3){
			$month = $month + 12;
			$year = $year - 1;
		}
		$jd = $day + floor((153 * $month - 457) / 5) + 365 * $year + floor($year / 4) - floor($year / 100) + floor($year / 400) + 1721118.5;
		return $jd;
	}
	function drawRhythm2($daysAlive, $period) {
		//global $daysToShow, $im, $imageWidth, $imageHeight;
		$centerDay = $daysAlive ;
		//$plotScale = ($diagramHeight - 25) / 2;
		//$plotCenter = ($diagramHeight - 25) / 2;
		$x=$centerDay;
		$phase = ($x % $period) / $period * 2 * pi();
		$y = sin($phase);
		$holla = round($y,2);
		return str_replace(".",",",$holla);
	}
	function parseFloat2($ptString) {
	    if (strlen($ptString) == 0) {
                    return false;
            }
	    $pString = str_replace(" ", "", $ptString);
	    $numberString = str_replace('.','',$pString);
	    $numberString = str_replace(',','.',$numberString);
	    $result = (double)$numberString;
            return $result;
	}
	function drawRhythm4($daysAlive) {
		//global $daysToShow, $im, $imageWidth, $imageHeight;
		$centerDay = $daysAlive ;
		//$plotScale = ($diagramHeight - 25) / 2;
		//$plotCenter = ($diagramHeight - 25) / 2;
		$a=22;
		$b=28;
		$c=33;
		$x=$centerDay;
		$phase1 = ($x % $a) / $a * 2 * pi();
		$phase2 = ($x % $b) / $b * 2 * pi();
		$phase3 = ($x % $c) / $c * 2 * pi();
		$y1 = sin($phase1);
		$y2 = sin($phase2);
		$y3 = sin($phase3);
		$y = ($y1+$y2+$y3)/3;
		$holla = round($y,2);
		return str_replace(".",",",$holla);
	}
	function dni($in) {
      global $lng;
      switch ($lng){
      case 'ru':
	if($in%10===1){
		return 'день';
	}elseif($in===1){
		return 'день';
	}elseif($in===2&&$in===3&&$in===4){
		return 'дня';
	}elseif($in%10>9&&$in%10<20){
		return 'дней';
	}elseif($in>9&&$in<20){
		return 'дней';
	}elseif($in%10===2){
		return 'дня';
	}elseif($in%10===3){
		return 'дня';
	}elseif($in%10===4){
		return 'дня';
	}elseif($in%10===5){
		return 'дней';
	}elseif($in%10===6){
		return 'дней';
	}elseif($in%10===7){
		return 'дней';
	}elseif($in%10===8){
		return 'дней';
	}elseif($in%10===9){
		return 'дней';
	}else{
		return 'дней';
	}
      break;
      case 'lt':
        if($in%10===1){
		return 'dienų';
	}elseif($in===1){
		return 'dieną';
	}elseif($in===2&&$in===3&&$in===4){
		return 'dienos';
	}elseif($in%10>9&&$in%10<20){
		return 'dienų';
	}elseif($in>9&&$in<20){
		return 'dienų';
	}elseif($in%10===2){
		return 'dienos';
	}elseif($in%10===3){
		return 'dienos';
	}elseif($in%10===4){
		return 'dienos';
	}elseif($in%10===5){
		return 'dienos';
	}elseif($in%10===6){
		return 'dienos';
	}elseif($in%10===7){
		return 'dienos';
	}elseif($in%10===8){
		return 'dienos';
	}elseif($in%10===9){
		return 'dienos';
	}else{
		return 'dienų';
	}  
      break;
      case 'pl':
        if($in%10===1){
		return 'dzien';
	}elseif($in===1){
		return 'dzien';
	}elseif($in===2&&$in===3&&$in===4){
		return 'dni';
	}elseif($in%10>9&&$in%10<20){
		return 'dni';
	}elseif($in>9&&$in<20){
		return 'dni';
	}elseif($in%10===2){
		return 'dni';
	}elseif($in%10===3){
		return 'dni';
	}elseif($in%10===4){
		return 'dni';
	}elseif($in%10===5){
		return 'dni';
	}elseif($in%10===6){
		return 'dni';
	}elseif($in%10===7){
		return 'dni';
	}elseif($in%10===8){
		return 'dni';
	}elseif($in%10===9){
		return 'dni';
	}else{
		return 'dni';
	}  
      break;
      case 'en':
        if($in%10===1){
		return 'days';
	}elseif($in===1){
		return 'day';
	}elseif($in===2&&$in===3&&$in===4){
		return 'days';
	}elseif($in%10>9&&$in%10<20){
		return 'days';
	}elseif($in>9&&$in<20){
		return 'days';
	}elseif($in%10===2){
		return 'days';
	}elseif($in%10===3){
		return 'days';
	}elseif($in%10===4){
		return 'days';
	}elseif($in%10===5){
		return 'days';
	}elseif($in%10===6){
		return 'days';
	}elseif($in%10===7){
		return 'days';
	}elseif($in%10===8){
		return 'days';
	}elseif($in%10===9){
		return 'days';
	}else{
		return 'days';
	} 
      break;
      }
}
	function TakeDates($days,$div) {
	    $mas = array();
	    $i=$days;
	    while($i<42000||count($mas)<11) {
		  if($i%$div==0) {
			$mas[]=$i;  
		  }
		  $i++;
	    }
	    return $mas;
      }
      function DayLeft_Date($cdate,$days) {
	    return strtotime("+".($cdate-$days)." day");
      }
	function AddLang($l,$d) {
	    if($l=='ru'){
		  $text =array('День рождения','Дней','Вам','через',''.dni($d),'Мне','А Вам?');
	    }elseif($l=='en'){
		  $text =array('Birth day','Day','You have','will be after',''.dni($d),'I have','And You ?');
	    }elseif($l=='lt'){
		  $text =array('Data urodzin','Dzien','Jums','sukaks po',''.dni($d),'Man jau','O Jums?');
	    }elseif($l=='pl'){
		  $text =array('Gimtadienis','Diena','Wam jest','bedzie za',''.dni($d),'Mnie jest','A ile Tobie?');
	    }
	    return $text;
	}
	if(isset($days))
	{
	    $hungred=TakeDates($days,100);
	    $thousand=TakeDates($days,1000);
	}
	if(isset($lng)&&isset($hungred)&&isset($thousand))
	{
		$mas=array();
		$mas=AddLang($lng,$days);
		$font = 'fonts/FreeSans.ttf';
		$imageWidth=300;
		$imageHeight=100;
		$size=15;
		$daysToShow=20;
		$str1='';
		$str2='';
		$site="http://poplauki.eu/bio";
		
		header("Content-type: image/png");
		$im = ImageCreate($imageWidth, $imageHeight)or die("Cannot Initialize new GD image stream");
		$bg=ImageColorAllocate($im, 255, 255, 255);
		$black = ImageColorAllocate($im, 0, 0, 0);
		$colorPhysical     = ImageColorAllocate($im, 0, 0, 255);
		$colorEmotional    = ImageColorAllocate($im, 255, 0, 0);
		$colorIntellectual = ImageColorAllocate($im, 15, 93, 22);
		$orange            = ImageColorAllocate($im, 210, 193, 77);
		if(count($hungred)>0){ 
                    ImageTTFText($im, 11, 0, 5, 20, $black,$font,''.$hungred[0].' '.$mas[3].' '.($hungred[0]-$days).' '.dni($hungred[0]-$days));
                    $ile=DayLeft_Date($hungred[0],$days);
                    if( strcmp($lng,'ru') === 0 ) {
                        ImageTTFText($im, 11, 0, 50, 35, $black,$font,' '.date('d.m.Y',$ile).'');
                    } elseif( strcmp($lng,'en') === 0 ) {
                        ImageTTFText($im, 11, 0, 50, 35, $black,$font,' '.date('d/m/Y',$ile).'');
                    } elseif( strcmp($lng,'pl') === 0 ) {
                        ImageTTFText($im, 11, 0, 50, 35, $black,$font,' '.date('d.m.Y',$ile).'');
                    } elseif( strcmp($lng,'lt') === 0 ) {
                        ImageTTFText($im, 11, 0, 50, 35, $black,$font,' '.date('Y-m-d',$ile).'');
                    }
		}
		if(count($thousand)>0){
                    ImageTTFText($im, 11, 0, 1, 60, $black,$font,' '.$thousand[0].' '.$mas[3].' '.($thousand[0]-$days).' '.dni($thousand[0]-$days));
                    $ile=DayLeft_Date($thousand[0],$days);
                    if( strcmp($lng,'ru') === 0 ) {
                        ImageTTFText($im, 11, 0, 50, 75, $black,$font,' '.date('d.m.Y',$ile).'');
                    } elseif( strcmp($lng,'en') === 0 ) {
                        ImageTTFText($im, 11, 0, 50, 75, $black,$font,' '.date('d/m/Y',$ile).'');
                    } elseif( strcmp($lng,'pl') === 0 ) {
                        ImageTTFText($im, 11, 0, 50, 75, $black,$font,' '.date('d.m.Y',$ile).'');
                    } elseif( strcmp($lng,'lt') === 0 ) {
                        ImageTTFText($im, 11, 0, 50, 75, $black,$font,' '.date('Y-m-d',$ile).'');
                    }
		}
		
		
		/*Draw Border Info */
		ImageTTFText($im, 13, 0, 202, 60, $black,$font,$days.' '.$mas[4]);
		if(isset($r)&&($r==1)){
			ImageTTFText($im, 14, 0, 205, 25, $black,$font,$mas[5]);
			ImageTTFText($im, 13, 0, 210, 80, $black,$font,$mas[6]);
		}else{
			ImageTTFText($im, 14, 0, 205, 25, $black,$font,$mas[5]);
		}
		
                if( strcmp($lng,'ru') === 0 ) {
                    ImageTTFText($im, 7, 0, 140, $imageHeight-1, $black,$font,$mas[0].' '.$d.'.'.$m.'.'.$y.'    '.$days.' '.dni($days));
                } elseif( strcmp($lng,'en') === 0 ) {
                    ImageTTFText($im, 7, 0, 140, $imageHeight-1, $black,$font,$mas[0].' '.$d.'/'.$m.'/'.$y.'    '.$days.' '.dni($days));
                } elseif( strcmp($lng,'pl') === 0 ) {
                    ImageTTFText($im, 7, 0, 140, $imageHeight-1, $black,$font,$mas[0].' '.$d.'.'.$m.'.'.$y.'    '.$days.' '.dni($days));
                } elseif( strcmp($lng,'lt') === 0 ) {
                    ImageTTFText($im, 7, 0, 140, $imageHeight-1, $black,$font,$mas[0].' '.$y.'-'.$m.'-'.$d.'    '.$days.' '.dni($days));
                }
		ImageRectangle($im, 1, 1, $imageWidth -1, $imageHeight - 10 ,$black);
		ImageTTFText($im, 7, 0, 10, $imageHeight-1, $black,$font,$site);
		ImageColorTransparent($im,$bg);
		ImagePng($im);
		ImageDestroy($im);
	}else{
		exit();
	}
?>
