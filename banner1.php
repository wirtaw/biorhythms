<?php
	function gregorianToJD2($month, $day, $year){
		if($month < 3){
			$month = $month + 12;
			$year = $year - 1;
		}
		$jd = $day + floor((153 * $month - 457) / 5) + 365 * $year + floor($year / 4) - floor($year / 100) + floor($year / 400) + 1721118.5;
		return $jd;
	}
	function drawRhythm2($daysAlive, $period)
	{
		//global $daysToShow, $im, $imageWidth, $imageHeight;
		$centerDay = $daysAlive ;
		//$plotScale = ($diagramHeight - 25) / 2;
		//$plotCenter = ($diagramHeight - 25) / 2;
		$x=$centerDay;
		$phase = ($x % $period) / $period * 2 * pi();
		$y = sin($phase);
		$holla = round($y,2);
		return str_replace(".",",",$holla);
	}
	function parseFloat2($ptString)
	{
	    if (strlen($ptString) == 0) {
                    return false;
            }
	    $pString = str_replace(" ", "", $ptString);
	    $numberString = str_replace('.','',$pString);
	    $numberString = str_replace(',','.',$numberString);
	    $result = (double)$numberString;
            return $result;
	}
	function drawRhythm4($daysAlive)
	{
		//global $daysToShow, $im, $imageWidth, $imageHeight;
		$centerDay = $daysAlive ;
		//$plotScale = ($diagramHeight - 25) / 2;
		//$plotCenter = ($diagramHeight - 25) / 2;
		$a=22;
		$b=28;
		$c=33;
		$x=$centerDay;
		$phase1 = ($x % $a) / $a * 2 * pi();
		$phase2 = ($x % $b) / $b * 2 * pi();
		$phase3 = ($x % $c) / $c * 2 * pi();
		$y1 = sin($phase1);
		$y2 = sin($phase2);
		$y3 = sin($phase3);
		$y = ($y1+$y2+$y3)/3;
		$holla = round($y,2);
		return str_replace(".",",",$holla);
	}
	function dni($in){
		if($in%10==1){
			return 'день';
		}elseif($in==1){
			return 'день';
		}elseif($in==2&&$in==3&&$in==4){
			return 'дня';
		}elseif($in%10>9&&$in%10<20){
			return 'дней';
		}elseif($in>9&&$in<20){
			return 'дней';
		}elseif($in%10==2){
			return 'дня';
		}elseif($in%10==3){
			return 'дня';
		}elseif($in%10==4){
			return 'дня';
		}elseif($in%10==5){
			return 'дней';
		}elseif($in%10==6){
			return 'дней';
		}elseif($in%10==7){
			return 'дней';
		}elseif($in%10==8){
			return 'дней';
		}elseif($in%10==9){
			return 'дней';
		}else{
			return 'дней';
		}
	}
	function DrawArrow($image,$type,$zn,$color,$font){
	    switch($zn)
	    {
		  case 1:
			//ImageLine($image,195, 20+$type*19,195, 20+$type*19, $color);
			//ImageLine($image, 15,181, 20 ,156, $color);
			//ImageLine($image, 25,181, 20 ,156, $color);
			ImageTTFText($image, 12, 0, 195, 20+$type*19, $color,$font,'+');
		  break;
		  case 0:
			//ImageLine($image, 20,191, 20 ,161, $color);
			ImageTTFText($image, 12, 0, 196, 20+$type*19, $color,$font,'-');
		  break;
	    }
	}
	function AddLang($l,$d){
	    if($l=='ru'){
		  $text =array('День рождения','Дней','Ф','Э','И','О','Вам',''.dni($d),'Мне','А Вам?');
	    }elseif($l=='en'){
		  $text =array('Birth day','Day','P','E','I','O','You have','days','I have','And You ?');
	    }elseif($l=='lt'){
		  $text =array('Data urodzin','Dzien','F','E','I','O','Jums','dienų','Man jau','O Jums?');
	    }elseif($l=='pl'){
		  $text =array('Gimtadienis','Diena','F','E','I','B','Wam jest','dni','Mnie jest','A ile Tobie?');
	    }
	    return $text;
	}
	if(isset($_GET['lng'])){
	    $lng=htmlentities(trim($_GET['lng']));
	    if(!in_array($lng,array('ru','en','lt','pl')))unset($lng);
	}
	
	if(isset($_GET['r']))$r=intval($_GET['r']);
	if(isset($_GET['y']))$y=intval($_GET['y']);
	if(isset($_GET['m']))$m=intval($_GET['m']);
	if(isset($_GET['d']))$d=intval($_GET['d']);
	if(isset($y)&&isset($m)&&isset($d)&&checkdate($m,$d,$y))$days=abs(gregorianToJD2($m, $d, $y)-gregorianToJD2(date("m"),date("d"),date("Y")));
	if(isset($lng)&&isset($days))
	{
		$mas=array();
		$mas=AddLang($lng,$days);
		$font = 'fonts/FreeSans.ttf';
		$imageWidth=300;
		$imageHeight=100;
		$size=15;
		$daysToShow=20;
		$site="http://poplauki.eu/bio";
		/*Calculate Physics */
		$kl1=drawRhythm2($days,23);
		$p1=((parseFloat2($kl1)+1)*100)/2;
		$p1w=intval(($p1*150)/100)+20;
		if($p1w<10){$p1w=20;}elseif($p1w>170){$p1w=170;}
		/* Calculate Physics Change */
		$kl1c=drawRhythm2($days+1,23);
		if(((parseFloat2($kl1c)+1)*100)/2>$p1){$zn1=1;}else{$zn1=0;}
		/*Calculate Emocional */
		$kl2=drawRhythm2($days,28);
		$p2=((parseFloat2($kl2)+1)*100)/2;
		$p2w=intval(($p2*150)/100)+20;
		if($p2w<10){$p2w=20;}elseif($p2w>170){$p2w=170;}
		/*Calculate Emocional Change */
		$kl2c=drawRhythm2($days+1,28);
		if(((parseFloat2($kl2c)+1)*100)/2>$p2){$zn2=1;}else{$zn2=0;}
		/*Calculate Intelectual */
		$kl3=drawRhythm2($days,33);
		$p3=((parseFloat2($kl3)+1)*100)/2;
		$p3w=intval(($p3*150)/100)+20;
		if($p3w<10){$p3w=20;}elseif($p3w>170){$p3w=170;}
		/*Calculate Intelectual Change */
		$kl3c=drawRhythm2($days+1,33);
		if(((parseFloat2($kl3c)+1)*100)/2>$p3){$zn3=1;}else{$zn3=0;}
		/*Calculate Overall */
		$overwall=round((parseFloat2($kl1)+parseFloat2($kl2)+parseFloat2($kl3))/3,2);
		//$ower=drawRhythm4($days);
		//$ower=(($ower+1)*100)/2;
		//$overwall=0;
		$o=(($overwall+1)*100)/2;
		$ow=intval(($o*150)/100)+20;
		if($ow<10){$ow=20;}elseif($ow>170){$ow=170;}
		/*Calculate Overall Change*/
		$overwall=round((parseFloat2($kl1c)+parseFloat2($kl2c)+parseFloat2($kl3c))/3,2);
		if((($overwall+1)*100)/2>$o){$zn4=1;}else{$zn4=0;}
		header("Content-type: image/png");
		$im = ImageCreate($imageWidth, $imageHeight)or die("Cannot Initialize new GD image stream");
		$bg=ImageColorAllocate($im, 255, 255, 255);
		$black = ImageColorAllocate($im, 0, 0, 0);
		$colorPhysical     = ImageColorAllocate($im, 0, 0, 255);
		$colorEmotional    = ImageColorAllocate($im, 255, 0, 0);
		$colorIntellectual = ImageColorAllocate($im, 15, 93, 22);
		$orange            = ImageColorAllocate($im, 210, 193, 77);
		/*Draw Physic */
		ImageTTFText($im, 8, 0, 173, 20, $black,$font,round($p1).'%');
		DrawArrow($im,0,$zn1,$colorPhysical,$font);
		//$kl1.' '.$p1.' width '.$p1w
		ImageTTFText($im, 10, 0, 5, 20, $colorPhysical,$font,$mas[2]);
		Imagefilledrectangle($im,20,5,$p1w,21,$colorPhysical);
		ImageRectangle($im, 20, 5, 170, 21 ,$black);
		/*Draw Emocional */
		ImageTTFText($im, 8, 0, 173, 39, $black,$font,round($p2).'%');
		DrawArrow($im,1,$zn2,$colorEmotional,$font);
		//$kl2.' '.$p2.' width '.$p2w
		ImageTTFText($im, 10, 0, 5, 39, $colorEmotional,$font,$mas[3]);
		Imagefilledrectangle($im,20,24,$p2w,39,$colorEmotional);
		ImageRectangle($im, 20, 24, 170, 40 ,$black);
		/*Draw Intelectual */
		ImageTTFText($im, 8, 0, 173, 59, $black,$font,round($p3).'%');
		DrawArrow($im,2,$zn3,$colorIntellectual,$font);
		//.' '.$kl3.' width '.$p3w
		ImageTTFText($im, 10, 0, 5, 59, $colorIntellectual,$font,$mas[4]);
		Imagefilledrectangle($im,20,43,$p3w,59,$colorIntellectual);
		ImageRectangle($im, 20, 43, 170, 59 ,$black);
		/*Draw Overall */
		ImageTTFText($im, 8, 0, 173, 77, $black,$font,round($o).'%');
		DrawArrow($im,3,$zn4,$orange,$font);
		//$overwall.' '.$o.' width '.$ow
		ImageTTFText($im, 10, 0, 5, 77, $orange,$font,$mas[5]);
		Imagefilledrectangle($im,20,62,$ow,78,$orange);
		ImageRectangle($im, 20, 62, 170,78 ,$black);
		/*Draw Border Info */
		ImageTTFText($im, 13, 0, 202, 60, $black,$font,$days.' '.$mas[7]);
		if(isset($r)&&($r==1)){
			ImageTTFText($im, 14, 0, 205, 25, $black,$font,$mas[8]);
			ImageTTFText($im, 13, 0, 210, 80, $black,$font,$mas[9]);
		}else{
			ImageTTFText($im, 14, 0, 205, 25, $black,$font,$mas[6]);
		}
		ImageRectangle($im, 1, 1, $imageWidth -1, $imageHeight - 10 ,$black);
		ImageTTFText($im, 7, 0, 10, $imageHeight-1, $black,$font,$site);
		//ImageTTFText($im, 7, 0, 140, $imageHeight-1, $black,$font,$mas[0].' '.$y.'  '.$m.' '.$d.' '.$days);
		//ImageTTFText($im, 7, 0, 140, $imageHeight-1, $black,$font,$kl1.' '.$kl2.' '.$kl3.' '.$overwall.' '.$ower.' '.($kl1+$kl2+$kl3));
		//ImageTTFText($im, 7, 0, 140, $imageHeight-1, $black,$font,doubleval($kl1).' '.doubleval($kl2).' '.doubleval($kl3));
		//ImageTTFText($im, 7, 0, 140, $imageHeight-1, $black,$font,$p1.' '.$p2.' '.$p3);
		//ImageTTFText($im, 7, 0, 140, $imageHeight-1, $black,$font,$zn1.' '.$zn2.' '.$zn3.' '.$zn4);
		ImageColorTransparent($im,$bg);
		ImagePng($im);
		ImageDestroy($im);
	}else{
		exit();
	}
?>
