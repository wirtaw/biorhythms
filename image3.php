<?php
session_start();
require_once('conf/functions.php');
require_once('conf/cnfg.php');
$imageWidth=$diagramWidth+50;
$imageHeight=$diagramHeight+80;
$daysGone = abs(gregorianToJD2($birthMonth, $birthDay, $birthYear)-gregorianToJD2(date( "m"), date( "d"), date( "Y")));
header("Content-type: image/png");
$im = ImageCreate($imageWidth, $imageHeight)or die("Cannot Initialize new GD image stream");

$bg=ImageColorAllocate($im, $r, $g, $b);
$black = ImageColorAllocate($im, 0, 0, 0);
$orange            = ImageColorAllocate($im, 210, 193, 77);
$colorForegr       = ImageColorAllocate($im, 255, 255, 255);
$colorGrid         = ImageColorAllocate($im, 0, 0, 0);
$colorCross        = ImageColorAllocate($im, 0, 0, 0);
$colorPhysical     = ImageColorAllocate($im, 0, 0, 255);
$colorEmotional    = ImageColorAllocate($im, 255, 0, 0);
$colorIntellectual = ImageColorAllocate($im, 15, 93, 22);
$nrSecondsPerDay = 60 * 60 * 24;
$diagramDate = time() - ($daysToShow / 2 * $nrSecondsPerDay) + $nrSecondsPerDay;
for ($i = 1; $i < $daysToShow; $i++)
{
    $thisDate = getDate($diagramDate);
    $xCoord = (($diagramWidth+20) / $daysToShow) * $i+10;
    ImageLine($im, $xCoord, $diagramHeight +5, $xCoord,$diagramHeight , $colorGrid);
	 ImageLine($im, $xCoord,($diagramHeight+3) /2, $xCoord, ($diagramHeight+8) /2, $colorGrid);
    ImageString($im, 2, $xCoord - 4, $diagramHeight+2 ,$thisDate[ "mday"], $colorGrid);
    $diagramDate += $nrSecondsPerDay;
}
for($i = 1; $i < 20; $i++){
	$max=3;
	$period=2/20;
	$skaic=1-$period*$i;
	$yCoord = (($diagramHeight+30) / 20) * $i;
	ImageLine($im, (($diagramWidth+20) / 2)+5, $yCoord, (($diagramWidth+20) / 2)+10,$yCoord, $colorGrid);
	ImageString($im,2,(($diagramWidth+20) / 2) +18, $yCoord-15,$skaic, $colorGrid);
}
//$kl=drawRhythm4($daysGone);
$kl1=drawRhythm2($daysGone,23);
$kl2=drawRhythm2($daysGone,28);
$kl3=drawRhythm2($daysGone,33);
$ssd=$mas_lang['yours'].' '.$daysGone.' '.dni($daysGone,$_SESSION['visitor']['lang']);
$font_size=10;
ImageLine($im, 20, ($diagramHeight ) / 2, $diagramWidth+25,($diagramHeight ) / 2, $colorCross);
ImageLine($im, ($diagramWidth+40) / 2, 20, ($diagramWidth+40) / 2, $diagramHeight ,$colorCross);
ImageRectangle($im, 19, 20, $diagramWidth +26, $diagramHeight ,$colorGrid);
if( strcmp($_SESSION['visitor']['lang'],'ru') === 0 ) {
    imageTTFText($im, $font_size, 0, 5, 15, $colorCross,$font, $text[0].':'.$birthDay.'.'.$birthMonth.'.'.$birthYear);
    imageTTFText($im, $font_size, 0, 200,15, $colorCross,$font, $text[1].':'.date('d.m.Y'));
} elseif( strcmp($_SESSION['visitor']['lang'],'en') === 0 ) {
    imageTTFText($im, $font_size, 0, 5, 15, $colorCross,$font, $text[0].':'.$birthDay.'/'.$birthMonth.'/'.$birthYear);
    imageTTFText($im, $font_size, 0, 200,15, $colorCross,$font, $text[1].':'.date('d/m/Y'));
} elseif( strcmp($_SESSION['visitor']['lang'],'pl') === 0 ) {
    imageTTFText($im, $font_size, 0, 5, 15, $colorCross,$font, $text[0].':'.$birthDay.'.'.$birthMonth.'.'.$birthYear);
    imageTTFText($im, $font_size, 0, 200,15, $colorCross,$font, $text[1].':'.date('d.m.Y'));
} elseif( strcmp($_SESSION['visitor']['lang'],'lt') === 0 ) {
    imageTTFText($im, $font_size, 0, 5, 15, $colorCross,$font, $text[0].':'.$birthYear.'-'.$birthMonth.'-'.$birthDay);
    imageTTFText($im, $font_size, 0, 200,15, $colorCross,$font, $text[1].':'.date('Y-m-d'));
}
imageTTFText($im, $font_size, 0, $diagramWidth-200,15, $colorCross,$font,$ssd);
drawRhythm_2($daysGone, 23, $colorPhysical);
drawRhythm_2($daysGone, 28, $colorEmotional);
drawRhythm_2($daysGone, 33, $colorIntellectual);
//drawRhythm_3($daysGone, $orange);
$font_size=10;
imageTTFText($im, $font_size, 0,  (($diagramWidth+20) / 2)- 200, $imageHeight - 10, $colorPhysical,$font, $text[2]);
imageTTFText($im, $font_size-1, 0, (($diagramWidth+20) / 2), $imageHeight-10, $colorPhysical,$font,$kl1);
imageTTFText($im, $font_size, 0,  (($diagramWidth+20) / 2)-200, $imageHeight - 25, $colorEmotional,$font, $text[3]);
imageTTFText($im, $font_size-1, 0, (($diagramWidth+20) / 2), $imageHeight-25, $colorEmotional,$font,$kl2);
imageTTFText($im, $font_size, 0, (($diagramWidth+20) / 2)- 200, $imageHeight - 40, $colorIntellectual,$font, $text[4]);
imageTTFText($im, $font_size-1, 0, (($diagramWidth+20) / 2), $imageHeight-40, $colorIntellectual,$font,$kl3);
//imageTTFText($im, 12, 0,  (($diagramWidth+20) / 2)- 200, $imageHeight - 55, $orange,$font, $text[6]);
//imageTTFText($im, 10, 0,(($diagramWidth+20) / 2), $imageHeight-55, $orange,$font,$kl);
imageStringUp($im,2,$imageWidth-($size+10),$imageHeight-2,$text[5],$black);
ImagePng($im);
ImageDestroy ($im);
?>
