<?php
        error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
        $mas=explode('/',$_SERVER["PHP_SELF"]);
        $dir='';
       $dirstyle='';
       $dirimage='';
       //var_dump($mas);
       if(count($mas)>2) {
            $dir='../../conf/';
            $dirstyle='../';
        }elseif(count($mas)==1){
            $dir='../conf/';
            $dirstyle='';
        }
       foreach($mas as $key => $value) {
            if(strpos($value,'.php')!==false) {
                $pageself=$value;  
            } else {
                $dir.='/'.$value;
            }
            if(strcmp($value,'admin')!==false) {
                $dir='./../conf/';$dirstyle='./../';
            }
       }
       unset($mas);
      session_start();
      if(file_exists('conf/Lang.php')){
          include('conf/Lang.php');
          
      }
      if(file_exists('conf/cnfg.php')) {
          include ('conf/cnfg.php');
          
      }
      ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo ShoWord('title','Poplauki').' | Biorythms'?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Meta info -->
        <meta name="description" content="<?php echo $mas_lang['descr'];?>">
        <meta name="keywords" content="<?php echo $mas_lang['keyword'];?>">
        <meta name="author" content="<?php echo $mas_lang['author'];?>" />
        
        <!-- Bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/sticky-footer-navbar.css">
        <link rel="stylesheet" href="js/vendor/datepicker/css/bootstrap-datepicker3.min.css">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7L6JT"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N7L6JT');</script>
        <!-- End Google Tag Manager -->
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Biorythm</a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo ShoWord('select','Select lang').''?> <b class="caret"></b></a>
                <ul class="dropdown-menu" id="lang-select" data-lang="<?php echo $_SESSION['visitor']['lang']; ?>">
                  <?php
                    if(isset($_SESSION['lang'])) { 
                        if(is_array($_SESSION['lang']) && count($_SESSION['lang'])>0){
                            foreach($_SESSION['lang'] as $key => $value){
                                echo $value->Show_In_Bootstrap('./');
                            }
                        }

                     }
                   ?>
                </ul>
              </li>
              <li id="showform" style="display:none;"><a href="#" id="showformbutton" name="showformbutton"><?php echo $mas_lang['show_form_again'];?></a></li>
            </ul>
              <div class="nav navbar-nav navbar-right">
                  <li >
                      <a href="#" id="bioryhmInfo" data-toggle="popover" title="<?php echo $mas_lang['review'];?>" data-content="<?php echo $mas_lang['desrc2'];?>" data-placement="left" data-trigger="focus"><span class="glyphicon glyphicon-info-sign" style="font-size: 20px;" ></span></a>
                  </li>
                
              </div>
          </div><!--/.nav-collapse -->
        </div>
      </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1><?php echo $mas_lang['title'];?> <small><?php echo $mas_lang['calculate_bio'];?></small></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="alert alert-warning alert-dismissible fade in" id="biorythmWarnings" role="alert" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <div id="emptyFormData" style="display:block;"><strong><?php echo $mas_lang['empty_warning_title'];?></strong> <?php echo $mas_lang['empty_warning_description'];?></div>
                        <div id="wrongFormData" style="display:none;"><strong><?php echo $mas_lang['wrong_warning_title'];?></strong> <?php echo $mas_lang['wrong_warning_description'];?></div>
                        <div id="responseFormData" style="display:none;"><strong><?php echo $mas_lang['response_warning_title'];?></strong> <?php echo $mas_lang['response_warning_description'];?></div>
                    </div>
                    <div id="loaderBlock" class="height-400 text-center center-block" style="font-size: 40px;">
                        <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> <?php echo $mas_lang['loading'];?>
                    </div>
                    <form class="form-horizontal" id="form-start" action="javascript:return false;" method="post" style="display:none;">
                        <fieldset>
                        <!-- Form Name -->
                        <legend><?php echo $mas_lang['form_legend'];?></legend>

                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="textinput"><?php echo $mas_lang['name'];?></label>  
                              <div class="col-md-4">
                              <input id="textinput" name="textinput" type="text" placeholder="Incognito" class="form-control input-md">
                              <span class="help-block"><?php echo $mas_lang['name_help'];?></span>  
                              </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="birthdate"><?php echo $mas_lang['day_of_birth'];?></label>  
                              <div class="col-md-4">
                                  <div class="input-group date" id="datepicker">
                                      <input id="birthdate" name="birthdate" type="text" placeholder="DD/MM/YYYY" class="form-control input-md" required="">
                                      <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar">
                                        </span>
                                    </span>
                                  </div>
                                <span class="help-block"><?php echo $mas_lang['birthdate_help'];?></span>  
                              </div>
                            </div>
                            <!-- Button -->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="enterbutton"></label>
                              <div class="col-md-4">
                                  <button id="enterbutton" name="enterbutton" class="btn btn-default"><?php echo $mas_lang['calculate'];?></button>
                              </div>
                            </div>
                        </fieldset>
                    </form>
                    <div id="canvas" class="container-fluid">
                        <div class="row">
                            <div class="col-md-8" id="bioinfo"></div>
                            <div class="col-md-4" id="biodownload"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" id="bioimage"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" id="additional-panels">
                    
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                        <?php echo $mas_lang['copyright'];?>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default" id="panel-template" style="display:none;">
            <div class="panel-heading">
              <h3 class="panel-title">
                  <a data-toggle="collapse" href="#collapse1">Collapsible panel</a>
              </h3>
            </div>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="panel-body">
                  Panel content
                </div>
                <div class="panel-footer">
                  
                </div>
            </div>
          </div>
        <div style="display:none;">
            <input type="hidden" id="defaultYear" value="<?php echo $birthYear; ?>">
            <input type="hidden" id="defaultMonth" value="<?php echo $birthMonth; ?>">
            <input type="hidden" id="defaultDay" value="<?php echo $birthDay; ?>">
            <input type="hidden" id="nameId" value="<?php if(isset($name_id)) {echo $name_id;} ?>">
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>');</script>

        <script src="js/bootstrap.min.js"></script>
        <script src="js/vendor/datepicker/js/bootstrap-datepicker.min.js"></script>
        <?php
            switch ($_SESSION['visitor']['lang']){
                case 'en':
                        echo '<script src="js/vendor/datepicker/locales/bootstrap-datepicker.en-GB.min.js" charset="UTF-8"></script>';
                    break;
                case 'ru':
                        echo '<script src="js/vendor/datepicker/locales/bootstrap-datepicker.ru.min.js" charset="UTF-8"></script>';
                    break;
                case 'lt':
                        echo '<script src="js/vendor/datepicker/locales/bootstrap-datepicker.lt.min.js" charset="UTF-8"></script>';
                    break;
                case 'pl':
                        echo '<script src="js/vendor/datepicker/locales/bootstrap-datepicker.pl.min.js" charset="UTF-8"></script>';
                    break;
                }

            ?>
        <script src="js/plugin.js"></script>
        <?php 
            if (isset($datepicker)) {
                echo '<script>$(document).ready(function () {'
                .'$(\'#showform\').fadeIn().show();'
                .'$(\'#form-start\').fadeOut().hide();'
                . '$(\'#birthdate\').val(\''.$datepicker.'\');';
                if (isset($username)) {
                    echo '$(\'#textinput\').val(\''.$username.'\');';
                }  
                echo '$(\'#enterbutton\').trigger(\'click\');';
                echo '});</script>';
            }
        ?>
    </body>
</html>
