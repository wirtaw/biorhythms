$(document).ready(function () {
    $('#loaderBlock').fadeOut().hide();
    $('#form-start').fadeIn().show();
    $('[data-toggle="popover"]').popover();
    var language = $('#lang-select').attr('data-lang');
    var birthdate = $('#birthdate');
    if ('en' === language) {
        birthdate.attr('placeholder','DD/MM/YYYY');
    } else if ('ru' === language) {
        birthdate.attr('placeholder','DD.MM.YYYY');
    } else if ('lt' === language) {
        birthdate.attr('placeholder','YYYY-MM-DD');
    } else if ('pl' === language) {
        birthdate.attr('placeholder','DD.MM.YYYY');
    }
    var calc_date = new Date();
    var place_copy = $('#footer .col-sm-12');
    var place_copy_text = $.trim(place_copy.html());
    if ('' !== place_copy_text) {
        place_copy_text = place_copy_text.replace(/2016/g,calc_date.getFullYear());
    } else {
        place_copy_text = '&copy poplauki.eu '+calc_date.getFullYear();
    }
    place_copy.html(place_copy_text);
    $('#datepicker input').datepicker({
        startView: 2,
        maxViewMode: 3,
        daysOfWeekHighlighted: "0",
        todayHighlight: true,
        defaultViewDate: { 
            year: parseInt($('#defaultYear').val()), 
            month: parseInt($('#defaultMonth').val()), 
            day: parseInt($('#defaultDay').val())
        },
        language: language
    });
    
    $('#datepicker .input-group-addon').on('click', function() {
        $('#birthdate').trigger('click');
    });
    
    $("#form-start").submit( function(ev) {
        ev.preventDefault();
    });
    
    $("#bioryhmInfo").on('click', function(ev) {
        ev.preventDefault();
    });
    
    $('#enterbutton').on('click', function() {
        var warning_panel = $('#biorythmWarnings');
        var datepicker = $('#birthdate').val();
        var language = $('#lang-select').attr('data-lang');
        var mas = [];
        var params = {
            name: $('#textinput').val(),
            year: 0,
            month: 0,
            day: 0,
            nameId: 0
        }
        var name_id = $('#nameId').val();
        if ('undefined' !== typeof name_id && '' !== name_id) {
            params.nameId = name_id;
        }
        if ('' !== datepicker) {
            if ('en' === language) {
                if (-1 < datepicker.indexOf('/')) {
                    mas= datepicker.split('/');
                    params.year = parseInt(mas[2]);
                    params.month = parseInt(mas[0]);
                    params.day = parseInt(mas[1]);
                }
            } else if ('ru' === language) {
                if (-1 < datepicker.indexOf('.')) {
                    mas= datepicker.split('.');
                    params.year = parseInt(mas[2]);
                    params.month = parseInt(mas[1]);
                    params.day = parseInt(mas[0]);
                }
            } else if ('lt' === language) {
                if (-1 < datepicker.indexOf('-')) {
                    mas= datepicker.split('-');
                    params.year = parseInt(mas[0]);
                    params.month = parseInt(mas[1]);
                    params.day = parseInt(mas[2]);
                }
            } else if ('pl' === language) {
                if (-1 < datepicker.indexOf('.')) {
                    mas= datepicker.split('.');
                    params.year = parseInt(mas[2]);
                    params.month = parseInt(mas[1]);
                    params.day = parseInt(mas[0]);
                }
            }
            if (0 < mas.length) {
                loadRythms(params);
            } else {
                console.log('error wrong date format');
                warning_panel.find('div').each(function () {
                    $(this).hide();
                });
                $('#wrongFormData').show();
                warning_panel.fadeIn().show();
                $('#form-start').fadeIn().show();
                $('#canvas').html('').hide();
                $('#additional-panels').html('');
            }
        } else {
            console.log('error empty');
            warning_panel.find('div').each(function () {
                $(this).hide();
            });
            $('#emptyFormData').show();
            warning_panel.fadeIn().show();
            $('#form-start').fadeIn().show();
            $('#canvas').html('').hide();
            $('#additional-panels').html('');
        }
    });
    
    
    $('#startGame').click(function() {
        var params = {
            name: $('#name1').val(),
            year: parseInt($('#year :selected').val()),
            month: parseInt($('#month :selected').val()),
            day: parseInt($('#day :selected').val())
        };
        loadRythms(params);
    });
    
    $("#aforizms ul li").click(function(){
        $("#aforizms div").removeClass("active");
        $("#aforizms div."+$(this).attr('class')).addClass("active");
    });
    
    $('#showformbutton').click(function(ev) {
        ev.preventDefault();
        $('#showform').fadeOut().hide();
        $('#form-start').fadeIn().show();
    });
});
function loadRythms(params){
    if( params.year>0 && params.month>-1 && params.day>0){
        //console.log('input '+$name+ ' '+$year+ ' '+$month+ ' '+$day);
        $.ajax({
            type: "POST",
            dataType:"json",
            url: "calculate2.php",
            data: { 'name': params.name, 'year': params.year,'month': params.month,'day': params.day,'nameId': params.nameId },
            complete: function(obj) {
                if(obj.answer){
                    //console.log('complete ');
                    //var html= ""+obj.your_days;
                    //$('#canvas').html(''+html).show();
                }
            },
            success: function (obj, textStatus) {
                if(obj.answer){
                    //console.log('success ');
                    $('#showform').fadeIn().show();
                    $('#form-start').fadeOut().hide();
                    if(false === obj.setcookie){
                        createCookie("aforizms",obj.aforizmsid,1);
                    }
                    var str_hungred="";
                    if(obj.str_hungred){
                        str_hungred=""+obj.str_hungred;
                    }
                    var str_thousand="";
                    if(obj.str_thousand){
                        str_thousand=""+obj.str_thousand;
                    }
                    
                    //$('#canvas').css({"border":"thin solid #c0c0c0","border-radius":"10px","height":"1040px"}).html(''+html).show();
                    var infopanel = $('#bioinfo');
                    infopanel.html('');
                    $('<div/>').html(str_hungred).appendTo(infopanel);
                    $('<div/>').html(str_thousand).appendTo(infopanel);
                    //$('<div/>').attr('id','linkdiv').html(obj.static_link).appendTo(infopanel);
                    
                    var downloadpanel = $('#biodownload');
                    downloadpanel.html('');
                    //$('<button>').attr('onclick','location.href=\'biopic.php?year='+obj.year+'&month='+obj.month+'&day='+obj.day+'\'').html('<span class="glyphicon glyphicon-download-alt"></span>').appendTo(downloadpanel);
                    
                    var bioimage = $('#bioimage');
                    bioimage.html('');
                    $('<img>').attr('src','image3.php?year='+obj.year+'&month='+obj.month+'&day='+obj.day)
                            .addClass('img-responsive').attr('id','img1')
                            .attr('alt','Biorythm graph for '+obj.name+' http://poplauki.eu/bio/').appendTo(bioimage);
                    //$('.sidebar .subnav').css('height','1030px');
                    var panels = $('#additional-panels');
                    var panel_template = $('#panel-template').clone();
                    if('undefined' !== typeof obj.static_link) {
                        panel_template.attr('id','panel-url');
                        panel_template.find('.panel-title a').attr('href','#collapse-url')
                            .attr('aria-expanded',true).html(obj.static_link.title);
                        panel_template.find('.panel-collapse').attr('id','collapse-url')
                            .attr('aria-expanded',true).addClass('in');
                        panel_template.find('.panel-body').attr('id','panel-url-body').html('');
                        panel_template.find('.panel-footer').html(obj.static_link.title);
                        panel_template.appendTo(panels);
                        var body_url = $('#panel-url-body');
                        $('<pre/>').attr('id','panel-body-url-pre').appendTo(body_url);
                        var body_url_pre = $('#panel-body-url-pre');
                        $('<code/>').text(obj.static_link.url).appendTo(body_url_pre);
                        
                        $('#panel-url').fadeIn().show();
                    } else {
                        if ('undefined' !== typeof $('#panel-url')) {
                            $('#panel-url').remove();
                        }
                    }
                    var navigation = null;
                    var content = null;
                    
                    if('undefined' !== typeof obj.name_info) {
                        panel_template = $('#panel-template').clone();
                        panel_template.attr('id','panel-username');
                        panel_template.find('.panel-title a')
                                .attr('href','#collapse-username').html(obj.mas_lang.name_title);
                        panel_template.find('.panel-collapse').attr('id','collapse-username');
                        panel_template.find('.panel-body').attr('id','panel-username-body').html(obj.name_info);
                        panel_template.find('.panel-footer').html(obj.mas_lang.name_description);
                        panel_template.appendTo(panels);
                        $('#panel-username').fadeIn().show();
                    } else {
                        if ('undefined' !== typeof $('#panel-username')) {
                            $('#panel-username').remove();
                        }
                    }
                    if('undefined' !== typeof obj.birth_number_text) {
                        panel_template = $('#panel-template').clone();
                        panel_template.attr('id','panel-number');
                        panel_template.find('.panel-title a').attr('href','#collapse-number')
                                .html(obj.mas_lang.birthdate_sum_title);
                        panel_template.find('.panel-collapse').attr('id','collapse-number');
                        panel_template.find('.panel-body')
                                .attr('id','panel-number-body').html(obj.birth_number_text);
                        panel_template.find('.panel-footer')
                                .html(obj.mas_lang.birthdate_sum_description);
                        panel_template.appendTo(panels);
                        $('#panel-number').fadeIn().show();
                    }
                    if('undefined' !== typeof obj.aforizms) {
                        panel_template = $('#panel-template').clone();
                        panel_template.attr('id','panel-aforizms');
                        panel_template.find('.panel-title a').attr('href','#collapse-aforizms')
                                .html(obj.mas_lang.aforizms);
                        panel_template.find('.panel-collapse').attr('id','collapse-aforizms');
                        panel_template.find('.panel-body').attr('id','panel-aforizms-body').html('');
                        panel_template.find('.panel-footer').html(obj.mas_lang.three_aforizms);
                        panel_template.appendTo(panels);
                        
                        var i=1;
                        var txtx="";
                        var af_body = $('#panel-aforizms-body');
                        af_body.html('');
                        $('<ul/>').addClass('nav nav-tabs')
                                .attr('id','panel-aforizms-body-nav').appendTo(af_body);
                        navigation = $('#panel-aforizms-body-nav');
                        $('<div/>').addClass('tab-content')
                                .attr('id','panel-aforizms-body-content').appendTo(af_body);
                        content = $('#panel-aforizms-body-content');
                        for(var p in obj.aforizms) {
                            txtx=str_replace( "\\", "", obj.aforizms[p] );
                            //$('<p/>').html(txtx).appendTo(af);
                            $('<li/>').attr('id','li-'+p).appendTo(navigation);
                            var li = $('#li-'+p);
                            $('<a/>').attr('href','#tab-'+p).attr('data-toggle','tab')
                                    .html(''+i).appendTo(li);
                            $('<div/>').attr('id','tab-'+p).addClass('tab-pane fade').appendTo(content);
                            var tab = $('#tab-'+p);
                            $('<p/>').html(txtx).appendTo(tab);
                            if (1 === i) {
                                li.addClass('active');
                                tab.addClass('in active');
                            }
                            i++;
                        }
                        $('#panel-aforizms').fadeIn().show();
                    } else {
                        if ('undefined' !== typeof $('#panel-aforizms')) {
                            $('#panel-aforizms').remove();
                        }
                    }
                    if('undefined' !== typeof obj.table_with_stat) {
                        panel_template = $('#panel-template').clone();
                        panel_template.attr('id','panel-life-days');
                        panel_template.find('.panel-title a')
                                .attr('href','#collapse-life-days').html(obj.mas_lang.average_life_title);
                        panel_template.find('.panel-collapse').attr('id','collapse-life-days');
                        panel_template.find('.panel-body')
                                .attr('id','panel-life-days-body').html('');
                        panel_template.find('.panel-footer').html(obj.mas_lang.average_life_description);
                        panel_template.appendTo(panels);
                        var body_days = $('#panel-life-days-body');
                        $('<h4/>').html(obj.table_with_stat.header).appendTo(body_days);
                        var data = obj.table_with_stat.spent_days_in;
                        if ('undefined' !== typeof data) {
                            $('<ul/>').attr('id','spent-day-list').addClass('list-group').appendTo(body_days);
                            var body_days_list = $('#spent-day-list');
                            for(var item in data) {
                                $('<li/>').addClass('list-group-item').attr('id','days-list-item-'+item)
                                    .html('<span class="badge">'+data[item].count+' '+data[item].word+'</span> '+data[item].title+'').appendTo(body_days_list);
                            }
                        }
                        $('#panel-life-days').fadeIn().show();
                    } else {
                        if ('undefined' !== typeof $('#panel-life-days')) {
                            $('#panel-life-days').remove();
                        }
                    }
                    
                    if('undefined' !== typeof obj.banners) {
                        panel_template = $('#panel-template').clone();
                        panel_template.attr('id','panel-banner');
                        panel_template.find('.panel-title a')
                                .attr('href','#collapse-banner').html(obj.mas_lang.banners_title);
                        panel_template.find('.panel-collapse').attr('id','collapse-banner');
                        panel_template.find('.panel-body')
                                .attr('id','panel-banner-body').html(obj.mas_lang.banners_description);
                        panel_template.find('.panel-footer').html('');
                        panel_template.appendTo(panels);
                        var panel_body = $('#panel-banner-body');
                        panel_body.html('');
                        $('<ul/>').addClass('nav nav-tabs')
                                .attr('id','panel-banner-body-nav').appendTo(panel_body);
                        navigation = $('#panel-banner-body-nav');
                        $('<div/>').addClass('tab-content')
                                .attr('id','panel-banner-body-content').appendTo(panel_body);
                        content = $('#panel-banner-body-content');
                        i = 0;
                        for(var item in obj.banners) {
                            var banner = obj.banners[item];
                            $('<li/>').attr('id','li-'+item).appendTo(navigation);
                            var li = $('#li-'+item);
                            $('<a/>').attr('href','#tab-'+item).attr('data-toggle','tab')
                                    .html(''+banner.name).appendTo(li);
                            $('<div/>').attr('id','tab-'+item).addClass('tab-pane fade').appendTo(content);
                            var tab = $('#tab-'+item);
                            $('<h4/>').html(banner.name).appendTo(tab);
                            $('<img/>').attr('src',banner.img_src).attr('style','border:none').addClass('img-responsive')
                                    .attr('alt','Biorythm graph banner http://poplauki.eu '+banner.banner_descr).appendTo(tab);
                            $('<pre/>').attr('id','tab-pre-'+item).appendTo(tab);
                            var pre = $('#tab-pre-'+item);
                            $('<code/>').addClass('').attr('id','tab-code-'+item).text(banner.code).appendTo(pre);
                            if (0 === i) {
                                li.addClass('active');
                                tab.addClass('in active');
                            }
                            i++;
                        }
                        $('#panel-banner').fadeIn().show();
                    }
                    //console.log('height'+$('.sidebar .subnav').css('height'));
                } else {
                    var warning_panel = $('#biorythmWarnings');
                    warning_panel.find('div').each(function () {
                        $(this).hide();
                    });
                    $('#responseFormData').show();
                    warning_panel.fadeIn().show();
                    $('#form-start').fadeIn().show();
                    $('#canvas').html('').hide();
                    $('#additional-panels').html('');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var warning_panel = $('#biorythmWarnings');
                warning_panel.find('div').each(function () {
                    $(this).hide();
                });
                $('#responseFormData').show();
                warning_panel.fadeIn().show();
                $('#form-start').fadeIn().show();
                $('#canvas').html('').hide();
                $('#additional-panels').html('');
            }
        });
    } else {
        var warning_panel = $('#biorythmWarnings');
        warning_panel.find('div').each(function () {
            $(this).hide();
        });
        $('#wrongFormData').show();
        warning_panel.fadeIn().show();
        $('#form-start').fadeIn().show();
        $('#canvas').html('').hide();
        $('#additional-panels').html('');
    }
}
/*-----------------------------------------------------------------------------------------------------*/
/*|                                          COOKIES                               			   |*/
/*-----------------------------------------------------------------------------------------------------*/
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	} else {
            var expires = "";
        }
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)===' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function select_lang(let){
    var form = document.createElement("form");
    var tmp=null;
    form.action = self.location;
    form.method = "post";
    form.id = "__id__tmp_form_for_post_submit";
    tmp = document.createElement("input");
    tmp.type = "hidden";
    tmp.name = "lang_sel";
    tmp.value = let;
    form.appendChild(tmp);
    var datepicker = document.getElementById("birthdate").value;
    if ('undefined' !== typeof datepicker && '' !== datepicker) {
        tmp = document.createElement("input");
        tmp.type = "hidden";
        tmp.name = "datepicker";
        tmp.value = datepicker;
        form.appendChild(tmp);
        
        tmp = document.createElement("input");
        tmp.type = "hidden";
        tmp.name = "old_lang";
        tmp.value = document.getElementById("lang-select").getAttribute('data-lang');
        form.appendChild(tmp);
    }
    var textinput = document.getElementById("textinput").value;
    if ('undefined' !== typeof textinput && '' !== textinput) {
        tmp = document.createElement("input");
        tmp.type = "hidden";
        tmp.name = "textinput";
        tmp.value = textinput;
        form.appendChild(tmp);
    }
    document.body.appendChild(form);
    form.submit();
}

/* str replace */
function str_replace ( search, replace, subject ) { // Replace all occurrences of the search string with the replacement string
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Gabriel Paderni
 
    if(!(replace instanceof Array)){
        replace=new Array(replace);
        if(search instanceof Array){//If search is an array and replace is a string, then this replacement string is used for every value of search
            while(search.length>replace.length){
                replace[replace.length]=replace[0];
            }
        }
    }
 
    if(!(search instanceof Array))search=new Array(search);
    while(search.length>replace.length){//If replace has fewer values than search , then an empty string is used for the rest of replacement values
        replace[replace.length]='';
    }
 
    if(subject instanceof Array){//If subject is an array, then the search and replace is performed with every entry of subject , and the return value is an array as well.
        for(k in subject){
            subject[k]=str_replace(search,replace,subject[k]);
        }
        return subject;
    }
 
    for(var k=0; k<search.length; k++){
        var i = subject.indexOf(search[k]);
        while(i>-1){
            subject = subject.replace(search[k], replace[k]);
            i = subject.indexOf(search[k],i);
        }
    }
 
    return subject;
 
}