<?php
    session_start();
    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
require_once('conf/functions.php');
require_once('conf/cnfg.php');
$imageWidth=$diagramWidth+50;
$imageHeight=$diagramHeight+80;
if(isset($mas_lang)){
$daysGone = abs(gregorianToJD2($birthMonth,$birthDay,$birthYear)-gregorianToJD2(date("m"),date("d"),date("Y")));
//$daysGone =$_SESSION['days'];
header("Content-type: image/png");
header('Content-Disposition: attachment; filename="your_biorytm.png"');
$im = @ImageCreate($imageWidth, $imageHeight)or die("Cannot Initialize new GD image stream");
/*
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
   */

$bg=ImageColorAllocate($im, 255, 255, 255);
$black = ImageColorAllocate($im, 0, 0, 0);
$orange            = ImageColorAllocate($im, 210, 193, 77);
$colorForegr       = ImageColorAllocate($im, 255, 255, 255);
$colorGrid         = ImageColorAllocate($im, 0, 0, 0);
$colorCross        = ImageColorAllocate($im, 0, 0, 0);
$colorPhysical     = ImageColorAllocate($im, 0, 0, 255);
$colorEmotional    = ImageColorAllocate($im, 255, 0, 0);
$colorIntellectual = ImageColorAllocate($im, 15, 93, 22);
$nrSecondsPerDay = 60 * 60 * 24;
$diagramDate = time() - ($daysToShow / 2 * $nrSecondsPerDay) + $nrSecondsPerDay;
for ($i = 1; $i < $daysToShow; $i++)
{
    $thisDate = getDate($diagramDate);
    $xCoord = (($diagramWidth+20) / $daysToShow) * $i+10;
    ImageLine($im, $xCoord, $diagramHeight +5, $xCoord,$diagramHeight , $colorGrid);
	 ImageLine($im, $xCoord,($diagramHeight+3) /2, $xCoord, ($diagramHeight+8) /2, $colorGrid);
    ImageString($im, 3, $xCoord - 4, $diagramHeight+2 ,$thisDate[ "mday"], $colorGrid);
    $diagramDate += $nrSecondsPerDay;
}
for($i = 1; $i < 20; $i++){
	$max=3;
	$period=2/20;
	$skaic=1-$period*$i;
	$yCoord = (($diagramHeight+30) / 20) * $i;
	ImageLine($im, (($diagramWidth+20) / 2)+5, $yCoord, (($diagramWidth+20) / 2)+10,$yCoord, $colorGrid);
	ImageString($im,2,(($diagramWidth+20) / 2) +18, $yCoord-15,$skaic, $colorGrid);
}
//$kl=drawRhythm4($daysGone);
$kl1=drawRhythm2($daysGone,23);
$kl2=drawRhythm2($daysGone,28);
$kl3=drawRhythm2($daysGone,33);
if($lang=='ru'){$ssd=$mas_lang['yours'].' '.$daysGone.' '.dni($daysGone);}else{$ssd=$mas_lang['yours'].' '.$daysGone.' '.$mas_lang['days_wr'];}
ImageLine($im, 20, ($diagramHeight ) / 2, $diagramWidth+20,($diagramHeight ) / 2, $colorCross);
ImageLine($im, ($diagramWidth+40) / 2, 20, ($diagramWidth+40) / 2, $diagramHeight ,$colorCross);
ImageRectangle($im, 19, 20, $diagramWidth +20, $diagramHeight ,$colorGrid);
imageTTFText($im, 12, 0, 5, 15, $colorCross,$font, $text[0].':'.$birthYear.'.'.$birthMonth.'.'.$birthDay);
imageTTFText($im, 12, 0, 200,15, $colorCross,$font, $text[1].':'.date('Y.m.d'));
imageTTFText($im, 12, 0, $diagramWidth-200,15, $colorCross,$font,$ssd);
drawRhythm_2($daysGone, 23, $colorPhysical);
drawRhythm_2($daysGone, 28, $colorEmotional);
drawRhythm_2($daysGone, 33, $colorIntellectual);
//drawRhythm_3($daysGone, $orange);
imageTTFText($im, 12, 0,  (($diagramWidth+20) / 2)- 200, $imageHeight - 10, $colorPhysical,$font, $text[2]);
imageTTFText($im, 10, 0, (($diagramWidth+20) / 2), $imageHeight-10, $colorPhysical,$font,$kl1);
imageTTFText($im, 12, 0,  (($diagramWidth+20) / 2)-200, $imageHeight - 25, $colorEmotional,$font, $text[3]);
imageTTFText($im, 10, 0, (($diagramWidth+20) / 2), $imageHeight-25, $colorEmotional,$font,$kl2);
imageTTFText($im, 12, 0, (($diagramWidth+20) / 2)- 200, $imageHeight - 40, $colorIntellectual,$font, $text[4]);
imageTTFText($im, 10, 0, (($diagramWidth+20) / 2), $imageHeight-40, $colorIntellectual,$font,$kl3);
//imageTTFText($im, 12, 0,  (($diagramWidth+20) / 2)- 200, $imageHeight - 55, $orange,$font, $text[6]);
//imageTTFText($im, 10, 0,(($diagramWidth+20) / 2), $imageHeight-55, $orange,$font,$kl);
imageStringUp($im,2,$imageWidth-($size+10),$imageHeight-2,$text[5],$black);
//header("Content-Transfer-Encoding: binary");
//$len = filesize($im);
//header("Content-Length: your_biorytm.png");
	Imagepng($im);
	@readfile($im);
	ImageDestroy($im);
}else{
	die();
}
?>
