<?php

if(!empty($_POST)){
           
            if(isset($_POST['name']) && !empty($_POST['name'])){
                $name=trim($_POST['name']);
            }
            if(isset($_POST['year'])){
                $year=intval($_POST['year']);
            }
            if(isset($_POST['month'])){
                $month=intval($_POST['month']);
            }
            if(isset($_POST['day'])){
                $day=intval($_POST['day']);
            }
            
        }
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        
        
        session_start();
        
        if(file_exists('conf/cookies.inc.php')){
            include ('conf/cookies.inc.php');
        }
        if(file_exists('conf/cnfg.php')){include ('conf/cnfg.php');
        
        function gregorianToJD2($month, $day, $year){
            if($month < 3){
                $month = $month + 12;
                $year = $year - 1;
            }
            $jd = $day + floor((153 * $month - 457) / 5) + 365 * $year + floor($year / 4) - floor($year / 100) + floor($year / 400) + 1721118.5;
            return $jd;
        }
        function TakeDates($days,$div){
              $mas = array();
              $i=$days;
              while($i<42000||count($mas)<11)
              {
                    if($i%$div==0)
                    {
                        $mas[]=$i;  
                    }
                    $i++;
              }
              return $mas;
        }
        function GetNameById($my_name_id){
                $ans=null;
                $mas=array();
                $filename='data/names.json';
                if(file_exists($filename)){
                    $source=@file_get_contents($filename);
                    if(!empty($source)){
                        $mas=json_decode($source);
                        //var_dump($mas);
                        if(is_array($mas)){
                            foreach($mas as $key => $value){
                                //var_dump($value);
                                //$mas2[$value->id]=$value->phrase;
                                if(strcmp($value->id,$my_name_id)===0){
                                    $ans=array('name'=>$value->name,'description'=>$value->description);
                                }
                            }
                        }
                        
                    }
                }
                
                return $ans;
        }
        function GetNameInfo($my_name)
        {
                $ans=null;
                $mas=array();
                $filename='data/names.json';
                if(file_exists($filename)){
                    $source=@file_get_contents($filename);
                    if(!empty($source)){
                        $mas=json_decode($source);
                        //var_dump($mas);
                        if(is_array($mas)){
                            foreach($mas as $key => $value){
                                //var_dump($value);
                                //$mas2[$value->id]=$value->phrase;
                                if(strcmp($value->name,$my_name)===0){
                                    $ans=array('id'=>$value->id,'description'=>$value->description);
                                }
                            }
                        }
                        
                    }
                }
                
                return $ans;
        }
        function GetAllAforizms()
        {
                $ans=array();
                $mas=array();
                $mas2=array();
                $filename='data/aforizmy.json';
                if(file_exists($filename)){
                    $source=@file_get_contents($filename);
                    if(!empty($source)){
                        $mas=json_decode($source);
                        //var_dump($mas);
                        if(is_array($mas)){
                            foreach($mas as $key => $value){
                                //var_dump($value);
                                $mas2[$value->id]=$value->phrase;
                            }
                        }
                        $ans=array_merge($ans,$mas2);
                    }
                }

                return $ans;
        }
        function GetAforizms()
        {
                $ans=array();
                $mas=array();
                $mas2=array();
                $filename='data/aforizmy.json';
                if(file_exists($filename)){
                    $source=@file_get_contents($filename);
                    if(!empty($source)){
                        $mas=json_decode($source);
                        if(is_array($mas)){
                            foreach($mas as $key => $value){
                                $mas2[$value->id]=$value->phrase;
                            }
                        }
                    }
                }

                $rand_keys=range(0, count($mas2));
                shuffle($rand_keys);

	
                 $key1=$rand_keys[0];
                 $key2=$rand_keys[1];
                 $key3=$rand_keys[2];

                if(!empty($mas2) && isset($mas2[$key1]) && isset($mas2[$key2]) && isset($mas2[$key3])){
                        $ans[$key1]=str_replace(array("&quot;"),array('\"'),$mas2[$key1]);
                        $ans[$key2]=str_replace(array("&quot;"),array('\"'),$mas2[$key2]);
                        $ans[$key3]=str_replace(array("&quot;"),array('\"'),$mas2[$key3]);

                }
                return $ans;
        }
        function GetAforizmsByKeys($mas)
        {
                $ans=array();
                $rand_keys=explode(',',$mas);
                $mas=array();
                $mas2=array();
                $filename='data/aforizmy.json';
                if(file_exists($filename)){
                    $source=@file_get_contents($filename);
                    if(!empty($source)){
                        $mas=json_decode($source);
                        if(is_array($mas)){
                            foreach($mas as $key => $value){
                                $mas2[$value->id]=$value->phrase;
                            }
                        }
                    }
                }
               

                if(!empty($mas2) && !empty($rand_keys)){
                    $key1=$rand_keys[0];
                    $key2=$rand_keys[1];
                    $key3=$rand_keys[2];
                        $ans[$key1]=str_replace(array("&quot;"),array('\"'),$mas2[$key1]);
                        $ans[$key2]=str_replace(array("&quot;"),array('\"'),$mas2[$key2]);
                        $ans[$key3]=str_replace(array("&quot;"),array('\"'),$mas2[$key3]);

                }
                return $ans;
        }
        function SumNumbersOfBirthdate($year,$month,$day){
            $sumd=0;
            $num=$year;
            $n4=$num-floor($num/10)*10;
            $n3=floor($num-$n4)/10;
            $n3=$n3-floor($n3/10)*10;
            $n2=($num-$n3*10-$n4)/100;
            $n2=$n2-floor($n2/10)*10;
            $n1=($num-$n2*100-$n3*10-$n4)/1000;
            $sumd=$n1+$n2+$n3+$n4;
            if($month>9){
                    $num=$month;
                    $n2=$num-floor($num/10)*10;
                    $n1=floor(($num-$n2)/10);
                    $sumd=$sumd+$n1+$n2;
            }else{
                    $sumd=$sumd+$month;
            }
            if($day>9){
                    $num=$day;
                    $n2=$num-floor($num/10)*10;
                    $n1=floor(($num-$n2)/10);
                    $sumd=$sumd+$n1+$n2;
            }else{
                    $sumd=$sumd+$day;
            }
            if($sumd>9||$sumd!=11&&$sumd!=22){
                    $num=$sumd;
                    $n2=$num-floor($num/10)*10;
                    $n1=floor(($num-$n2)/10);
                    $sumd=$n1+$n2;
                    if($sumd>9||$sumd!=11&&$sumd!=22){
                            $num=$sumd;
                            $n2=$num-floor($num/10)*10;
                            $n1=floor(($num-$n2)/10);
                            $sumd=$n1+$n2;
                    }
            }
            $sumd=intval($sumd);
            return $sumd;
        }
        function leta($in){
         if(isset($_SESSION['visitor']['lang'])){$lang=$_SESSION['visitor']['lang'];}else{$lang='ru';}
         switch ($lang){
              case 'ru':
                if($in%10==1){
                        return 'год';
                }elseif($in%10==2){
                        return 'год';
                }elseif($in%10==3){
                        return 'года';
                }elseif($in%10==4){
                        return 'года';
                }elseif($in%10==5){
                        return 'лет';
                }elseif($in%10==6){
                        return 'лет';
                }elseif($in%10==7){
                        return 'лет';
                }elseif($in%10==8){
                        return 'лет';
                }elseif($in%10==9){
                        return 'лет';
                }elseif($in%10==0){
                        return 'лет';
                }elseif($in>9&&$in<20){
                        return 'лет';
                }
              break;
              case 'lt':
                if($in%10==1){
                        return 'metai';
                }elseif($in%10==2){
                        return 'metai';
                }elseif($in%10==3){
                        return 'metai';
                }elseif($in%10==4){
                        return 'metai';
                }elseif($in%10==5){
                        return 'metai';
                }elseif($in%10==6){
                        return 'metai';
                }elseif($in%10==7){
                        return 'metai';
                }elseif($in%10==8){
                        return 'metai';
                }elseif($in%10==9){
                        return 'metai';
                }elseif($in%10==0){
                        return 'metų';
                }elseif($in>9&&$in<20){
                        return 'metų';
                }
              break;
              case 'pl':
                if($in%10==1){
                        return 'god';
                }elseif($in%10==2){
                        return 'lata';
                }elseif($in%10==3){
                        return 'lata';
                }elseif($in%10==4){
                        return 'lat';
                }elseif($in%10==5){
                        return 'lat';
                }elseif($in%10==6){
                        return 'lat';
                }elseif($in%10==7){
                        return 'lat';
                }elseif($in%10==8){
                        return 'lat';
                }elseif($in%10==9){
                        return 'lat';
                }elseif($in%10==0){
                        return 'lat';
                }elseif($in>9&&$in<20){
                        return 'lat';
                }
              break;
              case 'en':
                return 'years';
              break;
         }
        }
        function dni($in,$lang){
        switch ($lang){
              case 'ru':
                if($in%10==1){
                        return 'день';
                }elseif($in==1){
                        return 'день';
                }elseif($in==2&&$in==3&&$in==4){
                        return 'дня';
                }elseif($in%10>9&&$in%10<20){
                        return 'дней';
                }elseif($in>9&&$in<20){
                        return 'дней';
                }elseif($in%10==2){
                        return 'дня';
                }elseif($in%10==3){
                        return 'дня';
                }elseif($in%10==4){
                        return 'дня';
                }elseif($in%10==5){
                        return 'дней';
                }elseif($in%10==6){
                        return 'дней';
                }elseif($in%10==7){
                        return 'дней';
                }elseif($in%10==8){
                        return 'дней';
                }elseif($in%10==9){
                        return 'дней';
                }else{
                        return 'дней';
                }
              break;
              case 'lt':
                if($in%10==1){
                        return 'dienų';
                }elseif($in==1){
                        return 'dieną';
                }elseif($in==2&&$in==3&&$in==4){
                        return 'dienos';
                }elseif($in%10>9&&$in%10<20){
                        return 'dienų';
                }elseif($in>9&&$in<20){
                        return 'dienų';
                }elseif($in%10==2){
                        return 'dienos';
                }elseif($in%10==3){
                        return 'dienos';
                }elseif($in%10==4){
                        return 'dienos';
                }elseif($in%10==5){
                        return 'dienos';
                }elseif($in%10==6){
                        return 'dienos';
                }elseif($in%10==7){
                        return 'dienos';
                }elseif($in%10==8){
                        return 'dienos';
                }elseif($in%10==9){
                        return 'dienos';
                }else{
                        return 'dienos';
                }  
              break;
              case 'pl':
                if($in%10==1){
                        return 'dzien';
                }elseif($in==1){
                        return 'dzien';
                }elseif($in==2&&$in==3&&$in==4){
                        return 'dni';
                }elseif($in%10>9&&$in%10<20){
                        return 'dni';
                }elseif($in>9&&$in<20){
                        return 'dni';
                }elseif($in%10==2){
                        return 'dni';
                }elseif($in%10==3){
                        return 'dni';
                }elseif($in%10==4){
                        return 'dni';
                }elseif($in%10==5){
                        return 'dni';
                }elseif($in%10==6){
                        return 'dni';
                }elseif($in%10==7){
                        return 'dni';
                }elseif($in%10==8){
                        return 'dni';
                }elseif($in%10==9){
                        return 'dni';
                }else{
                        return 'dni';
                }  
              break;
              case 'en':
                if($in%10==1){
                        return 'days';
                }elseif($in==1){
                        return 'day';
                }elseif($in==2&&$in==3&&$in==4){
                        return 'days';
                }elseif($in%10>9&&$in%10<20){
                        return 'days';
                }elseif($in>9&&$in<20){
                        return 'days';
                }elseif($in%10==2){
                        return 'days';
                }elseif($in%10==3){
                        return 'days';
                }elseif($in%10==4){
                        return 'days';
                }elseif($in%10==5){
                        return 'days';
                }elseif($in%10==6){
                        return 'days';
                }elseif($in%10==7){
                        return 'days';
                }elseif($in%10==8){
                        return 'days';
                }elseif($in%10==9){
                        return 'days';
                }else{
                        return 'days';
                } 
              break;
              }
        }
        function DayLeft_Date($cdate,$days)
        {
              return strtotime("+".($cdate-$days)." day");
        }
        function show_table_years($years,$days){
            if(isset($_SESSION['visitor']['lang'])&&$_SESSION['visitor']['lang']=='ru'){
                    $mas_lang['part_daylife']=array('Сон','Учёба','Работа ','Еда','Свободное время','Спорт','Чистили зубы','Ждали зелёный свет светофора','Выплакано слёз','Учёба  до 25 лет','Работа до 63 лет','Спорт до 63 лет','Потушено свечей на именином пироге');
                    $lang='ru';
                    $mas_lang['s']=' Из ';
            }elseif(isset($_SESSION['visitor']['lang'])&&$_SESSION['visitor']['lang']=='en'){
                    $mas_lang['part_daylife']=array('Sleep','Education','Work','Food','Free time','Sport','Brush you teeth','Waited green light','Cry tears ','Education  up to 25 years','Work up to  63 years','Sport up to  63 years','Ligth off birthsday cake');
                    $mas_lang['days_wr']=' days';
                    $lang='en';
                    $mas_lang['years_wr']=' years';
                    $mas_lang['s']=' From ';
                    $mas_lang['l']='l';
                    $mas_lang['lights']='lights';
            }elseif(isset($_SESSION['visitor']['lang'])&&$_SESSION['visitor']['lang']=='lt'){
                    $mas_lang['part_daylife']=array('Miegas ','Mokslas ','Darbas ','Valgymas ','Laisvalaikis ','Sportas ','Dantims valyti ','Laukete žalios šviesoforo šviesos ','Nuverkta ašaru ','Mokslai iki 25 metų','Darbas iki 63 metų','Sportas iki 63 metų','Užgesinta žvakių gimtadienio torte');
                    $mas_lang['days_wr']=' dienos';
                    $mas_lang['years_wr']=' metu';
                    $lang='lt';
                    $mas_lang['s']=' Iš ';
                    $mas_lang['l']='l';
                    $mas_lang['lights']='žvakiu';
            }elseif(isset($_SESSION['visitor']['lang'])&&$_SESSION['visitor']['lang']=='pl'){
                    $mas_lang['part_daylife']=array('Sen','Nauka ','Praca ','Posilki ','Wolny czas ','Sport ','Myć zęby','Czekając zielone światło','Wyplakano lez','Nauka do 25 łat','Praca do 63 łat','Sport do 63 łat','Sgaszono swiec na urodzinowym torcie');
                    $mas_lang['days_wr']=' dni';
                    $mas_lang['years_wr']=' łat';
                    $mas_lang['s']=' Z ';
                    $lang='pl';
                    $mas_lang['l']='l';
                    $mas_lang['lights']='swiec';
            }else{
                    $mas_lang['part_daylife']=array('Сон','Учёба','Работа ','Еда','Свободное время','Спорт','Чистили зубы','Ждали зелёный свет светофора','Выплакано слёз','Учёба  до 25 лет','Работа до 63 лет','Спорт до 63 лет','Потушено свечей на именином пироге');
                    $lang='ru';
            }
            if($lang=='ru'){$table='<table class="posit"><tr><td><span style="text-align:center;">Из <span id="red">'.$days.'</span> '.dni($days,$_SESSION['visitor']['lang']).'( <span id="red">'.$years.' '.leta($years).'</span>) :</span></td><td>дней</td></tr>';}else{
            $table='<table class="posit"><tr><td><span style="text-align:center;"> '.$mas_lang['s'].'<span id="red">'.$days.'</span> '.$mas_lang['days_wr'].'( <span id="red">'.$years.' '.$mas_lang['years_wr'].'</span>) :</span></td><td>'.$mas_lang['days_wr'].'</td></tr>';}
            $lights=0;
            for($i=1;$i<=$years;$i++){
                $lights+=$i;
            }
            if($years>0&&$years<=25){
                            $son=floor(($days*18)/48);
                            $uceba=floor(($days*9)/48);
                            $eda=floor(($days*5)/48);
                            $free=floor(($days*14)/48);
                            $tren=floor(($days*1)/48);
                            $slezy=floor(($days*25)/9131);
                            $zel=floor(($days*61.25)/9131);
                            $zuby=floor(($days*15)/9131);
                            
                            if($lang=='ru'){	
                                    $table.='<tr><td >Сон </td><td>'.$son.' '.dni($son,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Учёба </td><td>'.$uceba.' '.dni($uceba,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Еда </td><td>'.$eda.' '.dni($eda,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Свободное время </td><td>'.$free.' '.dni($free,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Спорт </td><td>'.$tren.' '.dni($tren,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Чистили зубы </td><td>'.$zuby.' '.dni($zuby,$_SESSION['visitor']['lang']).'</td></tr>';
                                    $table.='<tr><td >Ждали зелёный свет светофора</td><td>'.$zel.' '.dni($zel,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Потушено свечей на именином пироге </td><td>'.$lights.' свечей </td></tr>';
                                    $table.='<tr><td >Выплакано слёз </td><td>'.$slezy.' л </td></tr>';
                            }else{
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][0].' </td><td>'.$son.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][1].' </td><td>'.$uceba.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][3].' </td><td>'.$eda.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][4].' </td><td>'.$free.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][5].' </td><td>'.$tren.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][6].' </td><td>'.$zuby.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][7].' </td><td>'.$zel.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][9].' </td><td>'.$lights.' '.$mas_lang['lights'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][8].' </td><td>'.$slezy.' '.$mas_lang['l'].' </td></tr>';
                            }
                    }elseif($years>25&&$years<=63){
                            $days_y=$days-9131;
                            $uceba_y=floor((9131*9)/48);
                            $son=floor(($days_y*16)/48)+floor((9131*18)/48);
                            $rabota=floor(($days_y*20)/48);
                            $eda=floor(($days_y*5)/48)+floor((9131*5)/48);
                            $free=floor(($days_y*6)/48)+floor((9131*14)/48);
                            $tren=floor(($days_y*1)/48)+floor((9131*1)/48);
                            $slezy=25+floor(($days_y*40)/$days);
                            $zel=61+floor(($days_y*64.25)/$days);
                            $zuby=15+floor(($days_y*14)/$days);
                            //echo ' свободное y: '.$days_y.' m : '.$days.' z1 '.floor(($days*15)/9131).' '.floor(($days_y*14)/$days).'<br>';
                            if($lang=='ru'){
                                    $table.='<tr><td >Сон </td><td>'.$son.' '.dni($son,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Работа </td><td>'.$rabota.' '.dni($rabota,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Еда </td><td>'.$eda.' '.dni($eda,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Свободное время </td><td>'.$free.' '.dni($free,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Спорт </td><td>'.$tren.' '.dni($tren,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Учёба  до 25 лет</td><td>'.$uceba_y.' '.dni($uceba_y,$_SESSION['visitor']['lang']).'</td></tr>';
                                    $table.='<tr><td >Чистили зубы </td><td>'.$zuby.' '.dni($zuby,$_SESSION['visitor']['lang']).'</td></tr>';
                                    $table.='<tr><td >Ждали зелёный свет светофора</td><td>'.$zel.' '.dni($zel,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Потушено свечей на именином пироге </td><td>'.$lights.' свечей </td></tr>';
                                    $table.='<tr><td >Выплакано слёз </td><td>'.$slezy.' л </td></tr>';
                            }else{
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][0].' </td><td>'.$son.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][2].' </td><td>'.$son.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][3].' </td><td>'.$eda.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][4].' </td><td>'.$free.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][5].' </td><td>'.$tren.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][9].' </td><td>'.$tren.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][6].' </td><td>'.$zuby.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][7].' </td><td>'.$zel.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][12].' </td><td>'.$lights.' '.$mas_lang['lights'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][8].' </td><td>'.$slezy.' '.$mas_lang['l'].' </td></tr>';
                            }
                    }elseif($years>63){
                            $days_y=9131;
                            $days_o=23058;
                            $days_m=$days-$days_o;
                            $days_t=36526;
                            //echo 'y: '.$days_y.' m : '.$days_m.' o: '.$days_o.'<br>';
                            //echo ' сон y: '.$son_y.' m : '.$son_m.' o: '.$son_o.'<br>';
                            //echo ' еда y: '.$eda_y.' m : '.$eda_m.' o: '.$eda_o.'<br>';
                            //echo ' свободное y: '.$free_y.' m : '.$free_m.' o: '.$free_o.'<br>';
                            $tren=floor(($days_m*1)/48)+floor(($days_y*1)/48);
                            //echo ' спорт y: '.$tren_y.' m : '.$tren_m.' <br>';
                            $uceba=floor(($days_y*9)/48);
                            //echo ' учёба y: '.$uceba.'<br>';
                            $son=floor(($days_y*18)/48)+floor(($days_m*16)/48)+floor(($days_y*18)/48);
                            $rabota=floor(($days_m*20)/48);
                            $eda=floor(($days_y*4)/48)+floor(($days_m*5)/48)+floor(($days_y*5)/48);
                            $free=floor(($days_y*26)/48)+floor(($days_m*6)/48)+floor(($days_y*14)/48);
                            $slezy=65+floor(($days*5)/$days_t);
                            $zel=66+floor(($days*57)/$days_t);
                            $zuby=29+floor(($days*7)/$days_t);
                            //echo ' свободное y: '.$days_y.' m : '.$days_m.' o: '.$days_o.' s: '.$days.'<br>';
                            if($lang=='ru'){
                                    $table.='<tr><td >Сон </td><td>'.$son.' '.dni($son,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Еда </td><td>'.$eda.' '.dni($eda,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Свободное время </td><td>'.$free.' '.dni($free,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Работа до 63 лет </td><td>'.$rabota.' '.dni($rabota,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Спорт до 63 лет </td><td>'.$tren.' '.dni($tren,$_SESSION['visitor']['lang']).' </tr>';
                                    $table.='<tr><td >Учёба до 25 лет </td><td>'.$uceba.' '.dni($uceba,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Чистили зубы </td><td>'.$zuby.' '.dni($zuby,$_SESSION['visitor']['lang']).'</td></tr>';
                                    $table.='<tr><td >Ждали зелёный свет светофора</td><td>'.$zel.' '.dni($zel,$_SESSION['visitor']['lang']).' </td></tr>';
                                    $table.='<tr><td >Потушено свечей на именином пироге </td><td>'.$lights.' свечей </td></tr>';
                                    $table.='<tr><td >Выплакано слёз </td><td>'.$slezy.' л </td></tr>';
                            }else{
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][0].' </td><td>'.$son.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][3].' </td><td>'.$eda.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][4].' </td><td>'.$free.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][10].' </td><td>'.$rabota.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][11].' </td><td>'.$tren.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][9].' </td><td>'.$tren.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][6].' </td><td>'.$zuby.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][7].' </td><td>'.$zel.' '.$mas_lang['days_wr'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][12].' </td><td>'.$lights.' '.$mas_lang['lights'].' </td></tr>';
                                    $table.='<tr><td >'.$mas_lang['part_daylife'][8].' </td><td>'.$slezy.' '.$mas_lang['l'].' </td></tr>';
                            }
                    }
                    $table.='</table>';
                    return $table;
    }
        $answer=array();
        if(isset($year) && isset($month) && isset($day)){
            if(checkdate( $month,$day,$year)){
                $dayd=date('j');
                $monthd=date('n');
                $yeard=date('Y');
                
                $daysGone = abs(gregorianToJD2($month,$day, $year)-gregorianToJD2(date("m"), date("d"), date("Y")));
                $answer['month']=$month;
                $answer['day']=$day;
                $answer['year']=$year;
                $answer['days']=$daysGone;
                $mas_lang['days_wr']=''.dni($daysGone,$_SESSION['visitor']['lang']);
                
                $answer['number_sum_of_days']=SumNumbersOfBirthdate($year,$month,$day);
                if(!empty($birth_number_text) && isset($birth_number_text[$answer['number_sum_of_days']])){
                    $answer['birth_number_text']=$mas_lang['value_of_birth'].''.$birth_number_text[$answer['number_sum_of_days']];
                }
                
                $answer['your_days'] = ''.$mas_lang['your_days'].' '.$daysGone.' '.$mas_lang['days_wr'];
		$answer['hungred']=TakeDates($daysGone,100);
		$answer['thousand']=TakeDates($daysGone,1000);
                
                $str="";
                if(isset($answer['hungred'])&&count($answer['hungred'])>0){
                      $str.= ''.$answer['hungred'][0].' ';
                      if(isset($mas_lang['before'])){$str.= $mas_lang['before'];}
                      $str.=  ' '.($answer['hungred'][0]-$daysGone).' ';
                      $str.=  dni($answer['hungred'][0]-$daysGone,$_SESSION['visitor']['lang']);
                      $ile=DayLeft_Date($answer['hungred'][0],$daysGone);
                      $str.=  ' '.date('Y-m-d',$ile).' <br/>';
                }
                if(!empty($str)){
                    $answer['str_hungred']=$str;
                }
                $str="";
                if(isset($answer['thousand'])&&count($answer['thousand'])>0){
                      $str.=  ' '.$answer['thousand'][0].' ';
                      if(isset($mas_lang['before']))$str.=  $mas_lang['before'];
                      $str.=  ' '.($answer['thousand'][0]-$daysGone).' ';
                      $str.=  dni($answer['thousand'][0]-$daysGone,$_SESSION['visitor']['lang']);
                      $ile=DayLeft_Date($answer['thousand'][0],$daysGone);
                      $str.=  ' '.date('Y-m-d',$ile).'<br/>';
                }
                if(!empty($str)){
                    $answer['str_thousand']=$str;
                }
                
                $answer['downloadimage'] =$mas_lang['download'];
                $linksf=substr($_SERVER['REQUEST_URI'],0,strrpos($_SERVER['REQUEST_URI'],'/')+1);
                $answer['link']='http://'.$_SERVER['SERVER_NAME'].$linksf.'index.php?year='.$year.'&month='.$month.'&day='.$day;
                
                $str="";
                if(isset($mas_lang['banner1'])&&isset($mas_lang['banner1text'])&&isset($mas_lang['bannerdescr'])){
                            $str.= '<table cellspacing="2" cellpadding="2">
					<tr><td colspan="2">'.$mas_lang['banner1'];
                            //if(isset($font)&&  file_exists( $font)) echo $font;
                            $str.= '</td></tr>
					<tr>
						<td><img src="banner1.php?lng='.$_SESSION['visitor']['lang'].'&y='.$year.'&m='.$month.'&d='.$day.'" alt="'.$mas_lang['bannerdescr'].'" style="border:none;"> </td>
						<td><textarea id="kodban" cols="40" rows="5" style="padding-left:5px;margin:1px;" onclick="document.getElementById(\'kodban\').select();return true;"><a href="http://'.$_SERVER['SERVER_NAME'].$linksf.'" target="_blank" style="border:none;"><img src="http://'.$_SERVER['SERVER_NAME'].$linksf.'banner1.php?lng='.$_SESSION['visitor']['lang'].'&y='.$year.'&m='.$month.'&d='.$day.'" alt="'.$mas_lang['bannerdescr'].'" style="border:none;"></a></textarea> </td>
					</tr>
					<tr><td colspan="2"></td></tr>
					<tr>
						<td><img src="banner1.php?lng='.$_SESSION['visitor']['lang'].'&y='.$year.'&m='.$month.'&d='.$day.'&r=1" alt="'.$mas_lang['bannerdescr'].'" style="border:none;"> </td>
						<td><textarea id="kodban2" cols="40" rows="5" style="padding-left:5px;margin:1px;" onclick="document.getElementById(\'kodban2\').select();return true;"><a href="http://'.$_SERVER['SERVER_NAME'].$linksf.'" target="_blank" style="border:none;"><img src="http://'.$_SERVER['SERVER_NAME'].$linksf.'banner1.php?lng='.$_SESSION['visitor']['lang'].'&y='.$year.'&m='.$month.'&d='.$day.'&r=1" alt="'.$mas_lang['bannerdescr'].'" style="border:none;"></a></textarea> </td>
					</tr>
					<tr><td colspan="2"></td></tr>
					<tr>
						<td><img src="banner2.php?lng='.$_SESSION['visitor']['lang'].'&y='.$year.'&m='.$month.'&d='.$day.'" alt="'.$mas_lang['bannerdescr2'].'" style="border:none;"> </td>
						<td><textarea id="kodban3" cols="40" rows="5" style="padding-left:5px;margin:1px;" onclick="document.getElementById(\'kodban3\').select();return true;"><a href="http://'.$_SERVER['SERVER_NAME'].$linksf.'" target="_blank" style="border:none;"><img src="http://'.$_SERVER['SERVER_NAME'].$linksf.'banner2.php?lng='.$_SESSION['visitor']['lang'].'&y='.$year.'&m='.$month.'&d='.$day.'" alt="'.$mas_lang['bannerdescr'].'" style="border:none;"></a></textarea> </td>
					</tr>
					<tr><td colspan="2">'.$mas_lang['banner1text'].'</td></tr>
				  </table><br/>';
                            
                }
                if(!empty($str)){
                    $answer['str_banner']=$str;
                }
                $str="";
                $str=show_table_years(abs($yeard-$year),$daysGone);
                if(!empty($str)){
                    $answer['table_with_stat']=$str;
                }
                
                if(isset($name))
                {
                    $answer['name']=$name;
                    $info=GetNameInfo($name);
                    if(!empty($info)){
                        $answer['name_id']=$info['id'];
                        $answer['name_info']='<div class="name_title">'.$name.'</div>'.$info['description'];
                        $answer['link'].='&id='.$info['id'];
                    }
                }
                
                if(isset($name_id)){
                    $info=GetNameById($name);
                    if(!empty($info)){
                        $answer['name']=$info['name'];
                        $answer['name_info']='<div class="name_title">'.$info['name'].'</div>'.$info['description'];
                        $answer['link'].='&id='.$name_id;
                    }
                }
                
                $answer['static_link']=''.$mas_lang['link1'].' <input id="links" type="text" value="'.$answer['link'].'" onclick="document.getElementById(\'links\').select();return true;">';
                
                
                
                if(strcmp($_SESSION['visitor']['lang'],'ru')===0)
                {
                    //$answer['all_aforizms']=GetAllAforizms();
                    $answer['COOKIE']=$_COOKIE;
                    if (isset($_COOKIE['aforizms']))
                    { 
                        $aforizms=GetAforizmsByKeys(@$_COOKIE['aforizms']);
                        
                        $answer['aforizms']=$aforizms;
                    }else{
                        $aforizms=GetAforizms();
                        //createCookie('aforizms',implode(',',array_keys($aforizms)),1,'','poplauki.eu/bio');
                        if(SetCookieLive("aforizms",implode(',',array_keys($aforizms)),time()+86400,'/','http://poplauki.eu/bio',false,false)==false)
                        {
				createCookie("aforizms",implode(',',array_keys($aforizms)),time()+86400,'/','http://poplauki.eu/bio',false,false);
                                $answer['setcookie']=true;
			}else{
                            $answer['setcookie']=false;
                            $answer['aforizmsid']=implode(',',array_keys($aforizms));
                        }
                        $answer['aforizms']=$aforizms;
                    }
                }
                $answer['answer']=true;
            }else{
                $answer['answer']=false;
                $answer['error']='Wrong date';
            }
        }else{
           $answer['answer']=false; 
           $answer['error']='not post data';
        }
        echo json_encode($answer);
        }
?>
